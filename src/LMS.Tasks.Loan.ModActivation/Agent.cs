using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using LMS.LoanManagement.Client;
using LMS.Loan.Filters.Client;
using System.Linq;
using LMS.LoanManagement.Abstractions;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.ModActivation
{
    public class Agent : ScheduledAgent
    {
        #region Constructor

        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ILoanManagementClientServiceFactory loanManagementClientServiceFactory,
            IEventHubClientFactory eventHubFactory,
            ILoanFilterClientServiceFactory loanFilterClientServiceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory
           )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            LoanManagementClientServiceFactory = loanManagementClientServiceFactory;
            LoanFilterClientServiceFactory = loanFilterClientServiceFactory;
            TenantServiceFactory = tenantServiceFactory;
        }

        #endregion

        #region Variables

        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ILoanManagementClientServiceFactory LoanManagementClientServiceFactory { get; }
        private ILoanFilterClientServiceFactory LoanFilterClientServiceFactory { get; }

        #endregion

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var loanManagementService = LoanManagementClientServiceFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventHub = EventHubFactory.Create(reader);
            var loanFilterService = LoanFilterClientServiceFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Configuration service not initiated");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Configuration related to loan mod activation task was not found");
                return;
            }
            try
            {
                logger.Info("Started execution of loan mod activation task.");
                var loans = await loanFilterService.GetAllByStatus(Configuration.LoanStatus);

                if (loans == null || !loans.Any())
                {
                    logger.Info("Ended execution of loan mod activation task as no loans where found.");
                    return;
                }
                foreach (var loan in loans)
                {
                    try
                    {
                        var loanSchedules = await loanManagementService.GetLoanScheduleByLoanNumber(loan.LoanNumber);
                        if (loanSchedules == null || !loanSchedules.Any())
                            continue;

                        foreach (var loanSchedule in loanSchedules)
                        {
                            if (loanSchedule.IsCurrent)
                                continue;
                            if (loanSchedule.ModStatus == ModStatus.Created)
                            {
                                if (DateTime.Compare(loanSchedule.FundedDate.Time.Date, tenantTime.Now.Date) == 0)
                                {
                                    await loanManagementService.SetLoanModActive(loan.LoanNumber, loanSchedule.Id);
                                    logger.Info($"Activated loan mod for LoanNumber:  {loan.LoanNumber}");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Activation of loan mod failed for loan number: {loan.LoanNumber} ", ex);
                    }
                }
                logger.Info("Ended execution of loan mod activation task");
            }
            catch (Exception e)
            {
                logger.Error("Error while execution of loan mod activation task", e);
            }

        }
    }
}
