﻿using LendFoundry.Calendar.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;

#endif
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LMS.Payment.Processor.Client;
using LMS.LoanManagement.Client;
using LendFoundry.Foundation.Logging;
using LMS.LoanAccounting;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.PaymentSuccess
{
    public class PaymentAgentFactory : IPaymentAgentFactory
    {

        public PaymentAgentFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var accrual = Provider.GetService<IAccrualBalanceClientFactory>();
            //   var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var controlFileFactory = Provider.GetService<IControlFileFactory>();
            var loanManagementClientServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var productServiceFactory = Provider.GetService<IProductServiceFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory, accrual, paymentServiceClientFactory, eventHubClientFactory, loggerFactory, tenantServiceFactory, loanManagementClientServiceFactory, productServiceFactory, controlFileFactory);
        }
    }
}
