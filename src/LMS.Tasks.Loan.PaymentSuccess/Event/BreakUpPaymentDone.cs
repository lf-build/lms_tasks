﻿using LMS.Payment.Processor;

namespace LMS.Tasks.Loan.PaymentSuccess
{
    public class BreakUpPaymentDone
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public IFunding Response { get; set; }
     
    }
}
