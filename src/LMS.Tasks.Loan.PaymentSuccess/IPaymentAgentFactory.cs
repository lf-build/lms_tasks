﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.PaymentSuccess
{
    public interface IPaymentAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}