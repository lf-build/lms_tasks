using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LMS.Tasks.Loan.PaymentSuccess {
    public class Configuration : IDependencyConfiguration {

        public List<string> FundingStatus { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}