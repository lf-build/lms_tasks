using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.Security.Tokens;
using LMS.LoanAccounting;
using LMS.LoanManagement.Client;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.PaymentSuccess
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IAccrualBalanceClientFactory accrualServiceFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
        //    IDataAttributesClientFactory dataAttributesClientFactory,
            ILoanManagementClientServiceFactory loanManagementClientService,
            IProductServiceFactory productFactory,
             IControlFileFactory controlFileFactory
            )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            AccrualServiceFactory = accrualServiceFactory;
            LoggerFactory = loggerFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
            EventHubFactory = eventHubFactory;
            //      DataAttributesClientFactory = dataAttributesClientFactory;
            ProductFactory = productFactory;
            LoanManagementClientService = loanManagementClientService;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }
        private IAccrualBalanceClientFactory AccrualServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }
        private IControlFileFactory ControlFileFactory { get; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }
        //   private IDataAttributesClientFactory DataAttributesClientFactory { get; }
        private ILoanManagementClientServiceFactory LoanManagementClientService { get; set; }
        private IProductServiceFactory ProductFactory { get; set; }
        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var paymentService = PaymentServiceClientFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var accrual = AccrualServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var productService = ProductFactory.Create(reader);
            var loanManagement = LoanManagementClientService.Create(reader);

            var contolFile = ControlFileFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the Configuration related to PaymentSuccess");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Was not found the Configuration related to PaymentSuccess");
                return;
            }
            try
            {
                var controlFileObject = new ControlDetail();
                controlFileObject.TaskName = Settings.ServiceName;
                controlFileObject.StartTime = tenantTime.Today;
                controlFileObject.ExceptionDetails = new List<ExceptionDetail>();

                logger.Info("Started payment success task");
                var todaysDate = tenantTime.Now.UtcDateTime;
                //   logger.Info($"processing date: {todaysDate}");
                //var todaysDate = new DateTimeOffset(new DateTime(2018, 02, 06)).Date;


                var payments = await paymentService.GetFundingRequest(Configuration.FundingStatus);

                payments = payments.Where(x => x.PaymentScheduleDate.Time.Date <= todaysDate.Date && x.IsFileCreated == true && x.IsAccounted == false).ToList();
                controlFileObject.NoOfLoanProcess = payments.ToList().Count;
                var successCount = 0;
                var skipCount = 0;
                var failCount = 0;
                double totalAmount = 0;
                var productList = await productService.GetAll();
                var parameterList = new List<IProductParameters>();
                controlFileObject.LoanProcessList = new List<string>();
                controlFileObject.LoanSkipList = new List<string>();
                controlFileObject.LoanSuccessList = new List<string>();
                foreach (var param in productList)
                {
                    var loanProductPrameters = await productService.GetAllProductParameters(param.ProductId);
                    if (loanProductPrameters != null)
                        parameterList.AddRange(loanProductPrameters);
                }

                foreach (var payment in payments.ToList())
                {
                    try
                    {
                        controlFileObject.LoanProcessList.Add(payment.LoanNumber);

                        var loan = await loanManagement.GetLoanInformationByLoanNumber(payment.LoanNumber);
                        var param = parameterList.FirstOrDefault(x => x.ParameterId == "PAYMENT_PROCESS_DAYS");
                        if (param == null && string.IsNullOrWhiteSpace(param.Value))
                            throw new NotFoundException($"Product Parameter PAYMENT_PROCESS_DAYS not  found for productId - {loan.LoanProductId}");

                        var paymentProcessDays = Convert.ToInt16(param.Value);
                        // resubmit after business date
                        if (payment.FileCreatedDate.Time.AddDays(paymentProcessDays).Date <= todaysDate.Date)
                        {
                            var paymentDetails = await MakePayment(paymentService, tenantTime, accrual, eventHub, payment, todaysDate);
                            if (paymentDetails != null)
                            {
                                successCount++;
                                controlFileObject.LoanSuccessList.Add(payment.LoanNumber);
                                totalAmount = totalAmount + paymentDetails.PaymentAmount;
                            }
                            else
                            {
                                controlFileObject.LoanSkipList.Add(payment.LoanNumber);
                                skipCount++;
                            }
                        }
                        else
                        {
                            controlFileObject.LoanSkipList.Add(payment.LoanNumber);
                            skipCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        failCount++;
                        var exception = new ExceptionDetail();
                        exception.LoanNumber = payment.EntityId;
                        exception.Exception = ex.Message;
                        controlFileObject.ExceptionDetails.Add(exception);
                        logger.Error($"Error while applying payment for loan {payment.EntityId}", ex);
                    }

                }
                controlFileObject.NoOfLoanSkip = skipCount;
                controlFileObject.NoOfLoanFail = failCount;
                controlFileObject.NoOfLoanSuccess = successCount;
                controlFileObject.TotalAccountedAmount = totalAmount;
                contolFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject,tenant);
                logger.Info("Ended payment success task");
            }
            catch (Exception e)
            {
                logger.Error("Error while Payment Success Task", e);
            }

        }

        public async Task<IFunding> MakePayment(IPaymentProcessorService paymentService, ITenantTime tenantTime, IAccrualBalanceService accrual, IEventHubClient eventHub, IFunding payment, DateTime scheduleDate)
        {
            IPaymentReceivedRequest paymentReq = new PaymentReceivedRequest();
            paymentReq.ProcessingDate = scheduleDate.Date;
            paymentReq.EffectiveDate = payment.IsReSubmitted ? scheduleDate.Date : (payment.PaymentAccountDate != null ? payment.PaymentAccountDate.Time.UtcDateTime.Date : scheduleDate.Date);
            paymentReq.Action = payment.PaymentPurpose;
            paymentReq.Principal = payment.PrincipalAmount;
            paymentReq.Interest = payment.InterestAmount;
            paymentReq.TotalAmount = payment.PaymentAmount;
            paymentReq.AdditionalInterest = payment.AdditionalInterestAmount;
            if (payment.Fees != null && payment.Fees.Count > 0)
            {
                paymentReq.ProcessingDate = payment.PaymentType == Payment.Processor.PaymentType.Fee ? scheduleDate.Date : paymentReq.ProcessingDate;
                paymentReq.EffectiveDate = payment.PaymentType == Payment.Processor.PaymentType.Fee ? scheduleDate.Date : paymentReq.EffectiveDate;
                paymentReq.Fees = new List<LoanAccounting.IFee>();
                foreach (var fee in payment.Fees)
                {
                    LoanAccounting.IFee feeDetails = new LoanAccounting.Fee();
                    feeDetails.FeeAmount = fee.FeeAmount;
                    feeDetails.FeeName = fee.Id;
                    feeDetails.AppliedOnDate = tenantTime.Now.Date;
                    paymentReq.Fees.Add(feeDetails);
                }
                paymentReq.FeeAmount = payment.Fees.Sum(x => x.FeeAmount);
            }
            paymentReq.Description = "Auto Daily Payment";
            await accrual.ApplyPaymentWithTotalAmount(payment.LoanNumber, paymentReq);

            payment.RequestStatus = "paymentsuccess";
            payment.IsAccounted = true;
            payment.IsReSubmitted = false;
            payment.ActualAccountDate = new TimeBucket(tenantTime.Now);
            await Task.Run(() => paymentService.UpdateFundingData(payment));

            // TODO open issue : LMS.Payment.Processor.IUpdateFundingAttamptStatusRequest
            // TODO open issue : is an interface, but no implementation was found anywhere.
            // TODO open issue : this is causing a build error. Is there a missing commit?
            // TODO open issue : -- alok
            var attempt = await paymentService.UpdateAttamptStatus(
                payment.ReferenceId, new UpdateFundingAttemptStatusRequest()
                {
                    ProviderResponseStatus = "paymentsuccess",
                    Status = "paymentsuccess",
                    IsAccounted = true
                });

            await eventHub.Publish("PaymentApplied", new
            {
                EntityId = payment.LoanNumber,
                EntityType = "loan",
                Response = payment
            });

            return payment;
        }
    }
}
