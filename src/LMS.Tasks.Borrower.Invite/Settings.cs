﻿
using System;

namespace LMS.Tasks.Borrower.Invite
{
    public static class Settings
    {        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-borrower-invite";
        public static string EventName { get; } = Environment.GetEnvironmentVariable("TASK_EVENT_NAME") ?? "LoanOnboarded";
    }
}