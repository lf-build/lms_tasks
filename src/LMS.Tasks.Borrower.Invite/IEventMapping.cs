namespace LMS.Tasks.Borrower.Invite
{
    public interface IEventMapping
    {

         string Name { get; set; }
         string EntityId { get; set; }
         string EntityType { get; set; }
    }
}