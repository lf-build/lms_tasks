namespace LMS.Tasks.Borrower.Invite
{
    public class EventMapping : IEventMapping
    {

        public string Name { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
    }
}