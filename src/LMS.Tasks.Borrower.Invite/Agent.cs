﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using LendFoundry.Business.Applicant.India.Client;
using LMS.LoanManagement.Client;
using System;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LMS.Tasks.Borrower.Invite
{
    public class Agent : EventBasedAgent
    {
        public Agent
        (
            ITokenHandler tokenHandler,
            ITokenReaderFactory tokenReaderFactory,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory factory,
            ITenantServiceFactory tenantServiceFactory,
            IConfigurationServiceFactory configurationServiceFactory,
            ILoanManagementClientServiceFactory loanOnboardingService,
            IApplicantServiceClientFactory applicantService
            
        )
            : base(factory, tenantServiceFactory, configurationServiceFactory, Settings.ServiceName, Settings.EventName, tokenHandler, loggerFactory)
        {
            TokenReaderFactory = tokenReaderFactory;
            ApplicantServiceClientFactory = applicantService;
            LoanManagementClientServiceFactory = loanOnboardingService;
            ConfigurationFactory = configurationServiceFactory;

        }
        
        private ITokenReaderFactory TokenReaderFactory { get; }

        private ILogger Logger { get; set; }

        private ILoanManagementClientServiceFactory LoanManagementClientServiceFactory {get;}
        private IApplicantServiceClientFactory ApplicantServiceClientFactory {get;}


        private IConfigurationServiceFactory ConfigurationFactory { get; }


        public override async void Execute(EventInfo @event)
        {
            Logger = LoggerFactory.CreateLogger();
            Logger.Info($"Received event {@event.Name}, Sending invite to borrower.");
            try
            {

                var token = TokenHandler.Issue(@event.TenantId, Settings.ServiceName);
                var reader = TokenReaderFactory.Create(@event.TenantId, string.Empty);
                var applicantService = ApplicantServiceClientFactory.Create(reader);
                var loanOnboardingService = LoanManagementClientServiceFactory.Create(reader);
                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                Configuration configuration = configurationService.Get();
                if (configuration == null)
                {
                    Logger.Error($"Was not found the Configuration related to payment pull request for Tenant {@event.TenantId}");
                    return;
                }
                var loanNumber = configuration.Event.EntityId.FormatWith(@event);
                var entityType = configuration.Event.EntityType.FormatWith(@event);
                var loanDetail = await loanOnboardingService.GetLoanInformationByLoanNumber(loanNumber);
                if(loanDetail == null)
                {
                    throw new ArgumentException($"Loan details not found for loan {loanNumber}");
                }
                if(string.IsNullOrWhiteSpace(loanDetail.BorrowerId))
                {
                    throw new ArgumentException($"BorrowerId not available for loan {loanNumber}");
                }

                var borrowerDetail = await applicantService.GetApplicantByBorrowerId(loanDetail.BorrowerId);
                if(borrowerDetail == null)
                {
                    throw new ArgumentException($"Borrower detail not found for borrower {loanDetail.BorrowerId}");
                }

                if(string.IsNullOrWhiteSpace(borrowerDetail.UserId))
                {
                      var borrowerToken = await applicantService.CreateIdentityToken(loanDetail.BorrowerId);
                      if( borrowerToken == null)
                      {
                          throw new ArgumentException($"Borrower token not available");
                      }
                      if(string.IsNullOrWhiteSpace(borrowerToken.Token))
                      {
                          throw new ArgumentException($"Borrower token not available");
                      }
                      IBorrowerEmailData emailData = new BorrowerEmailData()
                      {
                            EntityType = entityType,
                            BorrowerEmail = borrowerDetail.PrimaryEmail.Email,
                            BorrowerId = borrowerDetail.BorrowerId,
                            BorrowerName = borrowerDetail.LegalBusinessName,
                            PortalUrl = configuration.PortalUrl + borrowerToken.Token,
                            TemplateName = configuration.EmailTemplateName,
                            TemplateVersion = configuration.EmailTemplateVersion

                      };
                    await loanOnboardingService.SendBorrowerInvite(emailData);
                    Logger.Info($"Email Sent to borrower {borrowerDetail.BorrowerId}");

                }else
                {
                    Logger.Info($"User already available for borrower {borrowerDetail.BorrowerId}");
                }

            }
            catch (Exception ex)
            {
                Logger.Error($"Problems executing function ", ex);
            }
        }
    }
}