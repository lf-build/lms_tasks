using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LMS.Tasks.Borrower.Invite
{
    public class Configuration : IDependencyConfiguration
    {
        public string PortalUrl {get;set;}
        public string EmailTemplateName {get;set;}
        public string EmailTemplateVersion {get;set;}

        public EventMapping Event {get;set;}
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}