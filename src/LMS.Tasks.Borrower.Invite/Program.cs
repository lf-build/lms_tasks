﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Business.Applicant.India.Client;
using LMS.LoanManagement.Client;

#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using System;
using System.Collections.Generic;
using LendFoundry.Tasks;

namespace LMS.Tasks.Borrower.Invite
{
    public class Program : LendFoundry.Foundation.Services.DependencyInjection
    {

        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTransient<IAgent, Agent>();
            services.AddLoanManagementService();
            services.AddApplicantService();
            return services;
        }

    }
}


