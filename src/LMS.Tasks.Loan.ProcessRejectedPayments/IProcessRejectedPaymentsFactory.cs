﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.ProcessRejectedPayments
{
    public interface IProcessRejectedPaymentsFactory
    {
        Agent Create(ITokenReader reader);
    }
}