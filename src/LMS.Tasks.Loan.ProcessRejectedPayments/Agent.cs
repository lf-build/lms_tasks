using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;
using LendFoundry.EventHub.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using Renci.SshNet;
using LMS.Tasks.Loan.ProcessRejectedPayments.Helpers;
using System.IO;
using System.Text;

namespace LMS.Tasks.Loan.ProcessRejectedPayments
{
    public class Agent : ScheduledAgent
    {

        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IControlFileFactory controlFileFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;
        }

        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IControlFileFactory ControlFileFactory { get; }

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventHub = EventHubFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error($"Was not found the configuration related to process rejected payments for {tenant}");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null || Configuration.CSVColumnFormat == null || 
                Configuration.CSVColumnFormat.Count <= 0 || Configuration.Provider == null ||
                string.IsNullOrWhiteSpace(Configuration.RejectedEventName))
            {
                logger.Error($"Missing configuration settings related to process rejected payments for {tenant}");
                return;
            }
            try
            {
                logger.Info($"Started processing rejected payments for {tenant}");

                var files = DownloadFiles(Configuration.Provider.InboxLocation, Configuration.Provider.DestinationFileLocation, Configuration, logger);
                // Fetch from Excel file
                await ReadFile(Configuration.Provider.InboxLocation, Configuration.Provider.DestinationFileLocation, Configuration, eventHub, logger, tenantTime); 
                
                foreach (var file in files)
                {
                    File.Delete(Path.Combine(Configuration.Provider.DestinationFileLocation, file));
                }
                
                logger.Info($"Ended processing rejected payments for {tenant}");
            }
            catch (Exception e)
            {
                logger.Error($"Error while processing rejected payments for {tenant}", e);
            }

        }

        private SftpClient GetSftpClient(Configuration configuration)
        {
            return new SftpClient(configuration.Provider.Host, configuration.Provider.Port, configuration.Provider.Username, configuration.Provider.Password);
        }

        private async Task ReadFile(string sourcePath, string destinationPath, Configuration configuration, IEventHubClient eventHub, ILogger logger, ITenantTime tenantTime)
        {
            if (string.IsNullOrWhiteSpace(sourcePath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(sourcePath));
            }

            if (string.IsNullOrWhiteSpace(destinationPath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));
            }

            var inboxLocation = sourcePath;

            using (var client = GetSftpClient(configuration))
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);
                Dictionary<PaymentRejectedDetail, string> failedProcessing = new Dictionary<PaymentRejectedDetail, string>();
                foreach (var file in client.ListDirectory(inboxLocation))
                {
                    if (file.IsRegularFile)
                    {
                        bool isFailed = false;
                        bool isError = false;
                        try
                        {
                            var fileContent = ValidateFile(file.Name, destinationPath, configuration, logger);
                            List<PaymentRejectedDetail> paymentRejectedDetails = fileContent
                                    .Skip(1)
                                    .Select(v => PaymentRejectedDetail.FromCsv(v, configuration.CSVColumnFormat))
                                    .ToList();
                            
                            foreach (var paymentRejectedDetail in paymentRejectedDetails)
                            {
                                try
                                {
                                    await RaiseEvent(paymentRejectedDetail, configuration, eventHub);
                                }
                                catch (Exception ex)
                                {
                                    failedProcessing.Add(paymentRejectedDetail, ex.Message);
                                }
                            }

                            var csvContent = CreateFailedProcessingFile(failedProcessing, configuration.CSVColumnFormat);
                            if(csvContent != null)
                            {
                                var csvByteArray = Encoding.ASCII.GetBytes(csvContent.ToString());
                                var csvMemoryStream = new MemoryStream(csvByteArray);
                                isFailed = true;
                                client.UploadFile(csvMemoryStream, Path.Combine(configuration.Provider.FailedLocation, configuration.FailedFileName  + "_" + tenantTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv"), true);
                            }
                        }
                        catch (Exception exception)
                        {
                            isError = true;
                            logger.Error($"Unable to process file {file.Name}", exception);
                        }
                        finally
                        {
                            var fileContent = File.ReadAllBytes(Path.Combine(destinationPath, file.Name));
                            var csvMemoryStream = new MemoryStream(fileContent);
                            client.UploadFile(csvMemoryStream, Path.Combine(configuration.Provider.ArchiveLocation, file.Name));
                            if(!isFailed && isError)
                            {
                                FileInfo fileInfo = new FileInfo(file.Name);
                                csvMemoryStream = new MemoryStream(fileContent);
                                var fileName = configuration.FailedFileName + "_(Invalid_Extension_Header_Content)" + "_" + tenantTime.Now.ToString("yyyyMMddHHmmssfff") + fileInfo.Extension;
                                client.UploadFile(csvMemoryStream, Path.Combine(configuration.Provider.FailedLocation, fileName));
                            }
                            client.DeleteFile(Path.Combine(sourcePath, file.Name));
                        }
                    }
                }
            }
        }
        
        private string[] ValidateFile(string fileName, string sourcePath, Configuration configuration, ILogger logger)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if(fileInfo.Extension != ".csv")
            {
                logger.Error($"Invalid file {fileName}");
                throw new Exception($"Invalid file {fileName}");
            }
            var fileContent = File.ReadAllLines(Path.Combine(sourcePath, fileName));
            if(fileContent == null || fileContent.Count() <= 1)
            {
                logger.Error($"No content in {fileName}");
                throw new Exception($"No content in {fileName}");
            }
            var headers = fileContent.FirstOrDefault();
            var expectedHeaders = string.Join(",", configuration.CSVColumnFormat);
            if(!string.Equals(headers, expectedHeaders, StringComparison.InvariantCultureIgnoreCase))
            {
                logger.Error($"Incorrect header format for {fileName}");
                throw new Exception($"Incorrect header format for {fileName}");
            }
            return fileContent;
        }

        private async Task RaiseEvent(PaymentRejectedDetail paymentRejectedDetail, Configuration configuration, IEventHubClient eventHub)
        {
            if(paymentRejectedDetail == null)
            {
                return;
            }
            await eventHub.Publish(configuration.RejectedEventName, new 
                                                                        {
                                                                            Reason = paymentRejectedDetail.Reason, 
                                                                            ReferenceNumber = paymentRejectedDetail.ReferenceNumber,
                                                                            Description = paymentRejectedDetail.Description,
                                                                            Instruction = new 
                                                                                { 
                                                                                    ReferenceNumber = paymentRejectedDetail.ReferenceNumber, 
                                                                                    InternalReferenceNumber = paymentRejectedDetail.InternalReferenceNumber, 
                                                                                    Type = paymentRejectedDetail.Type, 
                                                                                    Amount = paymentRejectedDetail.Amount, 
                                                                                    StandardEntryClassCode = paymentRejectedDetail.StandardEntryClassCode, 
                                                                                    EntryDescription = paymentRejectedDetail.EntryDescription, 
                                                                                    FundingSourceId = paymentRejectedDetail.FundingSourceId, 
                                                                                    Receiving = new 
                                                                                    {
                                                                                        AccountType = paymentRejectedDetail.AccountType, 
                                                                                        RoutingNumber = paymentRejectedDetail.RoutingNumber, 
                                                                                        AccountNumber = paymentRejectedDetail.AccountNumber, 
                                                                                        Name = paymentRejectedDetail.Name
                                                                                    },
                                                                                    ExecutionDate = paymentRejectedDetail.ExecutionDate,
                                                                                    ReturnCode = paymentRejectedDetail.ReturnCode
                                                                                } 
                                                                            });
        }

        private StringBuilder CreateFailedProcessingFile(Dictionary<PaymentRejectedDetail, string> failedProcessing, List<string> headers)
        {
            if(failedProcessing == null || failedProcessing.Count() <= 0)
            {
                return null;
            }
            var csv = new StringBuilder();
            headers.Add("Exception");
            var csvHeader = string.Join(",", headers);
            csv.AppendLine(csvHeader );
            foreach (var item in failedProcessing)
            {
                var row = string.Empty;
                foreach (var header in headers)
                {
                    if(!string.IsNullOrWhiteSpace(row))
                    {
                        row += ",";
                    }
                    row += GetPropValue(item, header);  
                }
                row += $",{item.Value}";
                csv.AppendLine(row);
            }
            return csv;
        }

        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public List<string> DownloadFiles(string sourcePath, string destinationPath, Configuration configuration, ILogger logger)
        {
            if (string.IsNullOrWhiteSpace(sourcePath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(sourcePath));
            }
            if (string.IsNullOrWhiteSpace(destinationPath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));
            }
                
            var fileNameList = new List<string>();
            var inboxLocation = sourcePath;

            using (var client = GetSftpClient(configuration))
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                foreach (var file in client.ListDirectory(inboxLocation))
                {
                    if (file.IsRegularFile)
                    {
                        try
                        {
                            FaultRetry.RunWithAlwaysRetry(
                                () =>
                                {
                                    if(!File.Exists(destinationPath))
                                    {
                                        Directory.CreateDirectory(destinationPath);
                                    }
                                    using (var fs = new FileStream(Path.Combine(destinationPath, file.Name), FileMode.Create))
                                    {
                                        client.DownloadFile(file.FullName, fs);
                                        fs.Close();
                                    }
                                });

                            fileNameList.Add(file.Name);
                        }
                        catch (Exception exception)
                        {
                            logger.Error("Unable to download file", exception);
                        }
                    }
                }
            }
            return fileNameList;
        }

    }
}
