using System;

namespace LMS.Tasks.Loan.ProcessRejectedPayments
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}