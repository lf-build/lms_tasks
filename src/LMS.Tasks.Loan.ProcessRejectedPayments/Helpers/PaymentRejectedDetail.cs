using System;
using System.Collections.Generic;

namespace LMS.Tasks.Loan.ProcessRejectedPayments.Helpers
{
    public class PaymentRejectedDetail
    {
        public string ReferenceNumber { get; set; }

        public int InternalReferenceNumber { get; set; }

        public string ReturnCode { get; set; }

        public DateTime ExecutionDate { get; set; }

        public string Reason { get; set; }
        
        public string Description { get; set; }
        
        public string Type { get; set; }

        public decimal Amount { get; set; }

        public string StandardEntryClassCode { get; set; }

        public string EntryDescription { get; set; }

        public string FundingSourceId { get; set; }

        public string AccountType { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public string Name { get; set; }

        public static PaymentRejectedDetail FromCsv(string csvLine, List<string> csvFormat)
        {
            string[] values = csvLine.Split(',');
            PaymentRejectedDetail paymentRejectedDetail = new PaymentRejectedDetail();
            if(csvFormat.IndexOf("ReferenceNumber") >= 0)
            {
                paymentRejectedDetail.ReferenceNumber = Convert.ToString(values[csvFormat.IndexOf("ReferenceNumber")]);
            }
            if(csvFormat.IndexOf("InternalReferenceNumber") >= 0)
            {
                paymentRejectedDetail.InternalReferenceNumber = Convert.ToInt32(values[csvFormat.IndexOf("InternalReferenceNumber")]);
            }
            if(csvFormat.IndexOf("ReturnCode") >= 0)
            {
                paymentRejectedDetail.ReturnCode = Convert.ToString(values[csvFormat.IndexOf("ReturnCode")]);
            }
            if(csvFormat.IndexOf("ExecutionDate") >= 0)
            {
                paymentRejectedDetail.ExecutionDate = Convert.ToDateTime(values[csvFormat.IndexOf("ExecutionDate")]);
            }
            if(csvFormat.IndexOf("Reason") >= 0)
            {
                paymentRejectedDetail.Reason = Convert.ToString(values[csvFormat.IndexOf("Reason")]);
            }
            if(csvFormat.IndexOf("Description") >= 0)
            {
                paymentRejectedDetail.Description = Convert.ToString(values[csvFormat.IndexOf("Description")]);
            }
            if(csvFormat.IndexOf("Type") >= 0)
            {
                paymentRejectedDetail.Type = Convert.ToString(values[csvFormat.IndexOf("Type")]);
            }
            else
            {
                paymentRejectedDetail.Type = "DebitFromSavings";
            }
            if(csvFormat.IndexOf("Amount") >= 0)
            {
                paymentRejectedDetail.Amount = Convert.ToDecimal(values[csvFormat.IndexOf("Amount")]);
            }
            if(csvFormat.IndexOf("StandardEntryClassCode") >= 0)
            {
                paymentRejectedDetail.StandardEntryClassCode = Convert.ToString(values[csvFormat.IndexOf("StandardEntryClassCode")]);
            }
            if(csvFormat.IndexOf("EntryDescription") >= 0)
            {
                paymentRejectedDetail.EntryDescription = Convert.ToString(values[csvFormat.IndexOf("EntryDescription")]);
            }
            if(csvFormat.IndexOf("FundingSourceId") >= 0)
            {
                paymentRejectedDetail.FundingSourceId = Convert.ToString(values[csvFormat.IndexOf("FundingSourceId")]);
            }
            if(csvFormat.IndexOf("AccountType") >= 0)
            {
                paymentRejectedDetail.AccountType = Convert.ToString(values[csvFormat.IndexOf("AccountType")]);
            }
            else
            {
                paymentRejectedDetail.AccountType = "Individual";
            }
            if(csvFormat.IndexOf("RoutingNumber") >= 0)
            {
                paymentRejectedDetail.RoutingNumber = Convert.ToString(values[csvFormat.IndexOf("RoutingNumber")]);
            }
            if(csvFormat.IndexOf("AccountNumber") >= 0)
            {
                paymentRejectedDetail.AccountNumber = Convert.ToString(values[csvFormat.IndexOf("AccountNumber")]);
            }
            if(csvFormat.IndexOf("Name") >= 0)
            {
                paymentRejectedDetail.Name = Convert.ToString(values[csvFormat.IndexOf("Name")]);
            }
            return paymentRejectedDetail;
        }
    }
}