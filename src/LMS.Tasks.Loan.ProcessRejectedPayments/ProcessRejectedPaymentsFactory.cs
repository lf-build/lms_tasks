﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.ProcessRejectedPayments
{
    public class ProcessRejectedPaymentsFactory : IProcessRejectedPaymentsFactory
    {

        public ProcessRejectedPaymentsFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var controlFileFactory = Provider.GetService<IControlFileFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory, eventHubClientFactory, loggerFactory, tenantServiceFactory, controlFileFactory);
        }
    }
}
