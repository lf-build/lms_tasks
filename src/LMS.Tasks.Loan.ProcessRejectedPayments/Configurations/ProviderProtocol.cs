namespace LMS.Tasks.Loan.ProcessRejectedPayments.Configurations
{
    public enum ProviderProtocol
    {
        Ftp=1,
        Sftp=2
    }
}