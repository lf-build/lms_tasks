﻿
using LendFoundry.Foundation.Client;
using LMS.Tasks.Loan.ProcessRejectedPayments.Configurations;
using System.Collections.Generic;

namespace LMS.Tasks.Loan.ProcessRejectedPayments
{
    public class Configuration : IDependencyConfiguration
    {
        public Provider Provider { get; set; }
        public List<string> CSVColumnFormat { get; set; }
        public string RejectedEventName { get; set; }
        public string FailedFileName { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}
