using LendFoundry.StatusManagement.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LMS.Loan.Filters.Client;
using LMS.LoanManagement.Client;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.DataAttributes.Client;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using LendFoundry.DataAttributes;
using LMS.LoanAccounting;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.ProductConfiguration;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.CreatePullRequest
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ILoanFilterClientServiceFactory loanFilterClientServiceFactory,
             ILoanManagementClientServiceFactory loanManagementClientService,
            IEventHubClientFactory eventHubFactory,
            ICalendarServiceFactory calendarServiceFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory,
            IDataAttributesClientFactory dataAttributesEngine,
            IAccrualBalanceClientFactory accrualServiceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IProductServiceFactory productFactory,
            IControlFileFactory controlFileFactory
           )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            LoanManagementClientService = loanManagementClientService;
            LoanFilterClientServiceFactory = loanFilterClientServiceFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
            CalendarServiceFactory = calendarServiceFactory;
            EventHubFactory = eventHubFactory;
            DataAttributesEngine = dataAttributesEngine;
            AccrualServiceFactory = accrualServiceFactory;
            ProductFactory = productFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;

        }
        
        #region Private Properties

        private IControlFileFactory ControlFileFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILoanFilterClientServiceFactory LoanFilterClientServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ICalendarServiceFactory CalendarServiceFactory { get; }
        private IDataAttributesClientFactory DataAttributesEngine { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IStatusManagementServiceFactory StatusManagementFactory { get; }
        private ILoanManagementClientServiceFactory LoanManagementClientService { get; set; }
        private IProductServiceFactory ProductFactory { get; set; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }
        private IAccrualBalanceClientFactory AccrualServiceFactory { get; }
        private static string EntityType = "loan";
        
        #endregion

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName, null, "system", null);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var loanSchedule = LoanManagementClientService.Create(reader);
            var loanFilterService = LoanFilterClientServiceFactory.Create(reader);
            var paymentService = PaymentServiceClientFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var calendar = CalendarServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var dataAttributeService = DataAttributesEngine.Create(reader);
            var accrual = AccrualServiceFactory.Create(reader);
            var controlFile = ControlFileFactory.Create(reader);
            var productService = ProductFactory.Create(reader);

            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the Configuration related to payment pull request Task");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Was not found the Configuration related to payment pull request");
                return;
            }
            try
            {
                logger.Info("Started Creating Payment Pull Request");
                var controlFileObject = new ControlDetail();
                controlFileObject.TaskName = Settings.ServiceName;
                controlFileObject.StartTime = tenantTime.Today;
                controlFileObject.ExceptionDetails = new List<ExceptionDetail>();
                controlFileObject.LoanProcessList = new List<string>();
                controlFileObject.LoanSkipList = new List<string>();
                controlFileObject.LoanSuccessList = new List<string>();

                var loans = await loanFilterService.GetAllByStatus(Configuration.LoanStatus);
                logger.Info($"ProcessingDate " , new {tenantTime.Today});
                var successCount = 0;
                var processCount = 0;
                var skipCount = 0;
                var failCount = 0;
                double totalAmount = 0;
                var productList = await productService.GetAll();
                var parameterList = new List<IProductParameters>();
                foreach (var param in productList)
                {
                    var loanProductParameters = await productService.GetAllProductParameters(param.ProductId);
                    if (loanProductParameters != null && loanProductParameters.Count > 0)
                        parameterList.AddRange(loanProductParameters);
                }
                foreach (var loan in loans.ToList())
                {
                    try
                    {
                        var param = parameterList.FirstOrDefault(x => x.ParameterId == "PAYMENT_PROCESS_DAYS" && x.ProductId == loan.LoanProductId);
                        if (param == null )
                            throw new NotFoundException($"Product Parameter PAYMENT_PROCESS_DAYS not  found for productId - {loan.LoanProductId}");

                        var notificationSendDays = Convert.ToInt16(param.Value);
                        var processingEndDate = tenantTime.Now.Date;
                        for (int i = 0; i < notificationSendDays; i++)
                        {
                            var todayInfo = calendar.GetDate(processingEndDate.Year, processingEndDate.Month, processingEndDate.Day);
                            processingEndDate = todayInfo.NextBusinessDay.Date;
                        }
                        var currentSchedule = await loanSchedule.GetLoanSchedule(loan.LoanNumber);
                        var processingStartDate = tenantTime.Now.Date;
                        var scheduleToBeProcess = currentSchedule.ScheduleDetails.Where(x => x.IsProcessed == false && x.IsMissed != true && x.ScheduleDate.Date > processingStartDate.Date && x.ScheduleDate.Date <= processingEndDate.Date).ToList();

                        foreach (var schedule in scheduleToBeProcess)
                        {
                            processCount++;
                            controlFileObject.LoanProcessList.Add(loan.LoanNumber);
                            logger.Info($"Schedule found for LoanNumber : {loan.LoanNumber} for date {schedule.ScheduleDate}");
                            if (schedule != null)
                            {
                                var paymentHierarchyName = !string.IsNullOrWhiteSpace(Configuration.PaymentHierarchyName) ? Configuration.PaymentHierarchyName : "default";
                                var paymentDetails = await PaymentPull(loanSchedule, paymentService, tenantTime, eventHub, dataAttributeService, accrual, Configuration, loan.LoanNumber, loan.IsAutoPay, currentSchedule, schedule, paymentHierarchyName);
                                if (paymentDetails != null)
                                {
                                    successCount++;
                                    controlFileObject.LoanSuccessList.Add(loan.LoanNumber);
                                    totalAmount = totalAmount + paymentDetails.PaymentAmount;
                                }
                                else
                                {
                                    controlFileObject.LoanSkipList.Add(loan.LoanNumber);
                                    skipCount++;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        failCount++;
                        var exception = new ExceptionDetail();
                        exception.LoanNumber = loan.LoanNumber;
                        exception.Exception = ex.Message;
                        controlFileObject.ExceptionDetails.Add(exception);
                        logger.Error($"Error while Processing Request for loan : {loan.LoanNumber}", ex);
                    }
                }
                controlFileObject.NoOfLoanProcess = processCount;
                controlFileObject.NoOfLoanSkip = skipCount;
                controlFileObject.NoOfLoanFail = failCount;
                controlFileObject.NoOfLoanSuccess = successCount;
                controlFileObject.TotalPulledAmount = totalAmount;
                controlFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject,tenant);

                logger.Info("Ended Creating Payment Pull Request");

            }
            catch (Exception e)
            {
                logger.Error("Error while generating ACH Instructions", e);
            }
        }

        public async Task<IFunding> PaymentPull(ILoanManagementClientService loanOnboardingService, IPaymentProcessorService paymentService, ITenantTime tenantTime, IEventHubClient eventHub, IDataAttributesEngine dataAttributeService, IAccrualBalanceService accrual, Configuration Configuration, string loanNumber, bool IsAutoPay, ILoanSchedule currentSchedule, ILoanScheduleDetails schedule, string paymentHierarchyName)
        {
            var bankDetail = await loanOnboardingService.GetBankDetails(loanNumber);
            var loanDetails = await loanOnboardingService.GetLoanInformationByLoanNumber(loanNumber);
            var instrument = loanDetails.PaymentInstruments.FirstOrDefault(x => x.Id == bankDetail.PaymentInstrumentId);
            if (instrument == null)
            {
                throw new NotFoundException($"Payment InstrumentId {bankDetail.PaymentInstrumentId} is not found in loan details");
            }
            double principalAmount = 0;
            double interestAmount = 0;
            double additionalInterest = 0;
            double paymentAmount = 0;
            var feeDetails = new List<Payment.Processor.IFeeDetails>();

            if (Configuration.UsePaymentHierarchy)
            {
                var product = await GetProductDetail(loanNumber, dataAttributeService);
                var paymentHierarchy = product.Product?.PaymentHierarchy.FirstOrDefault(x => x.Purpose == paymentHierarchyName);
                var accrualDetails = await accrual.GetLoanByScheduleVersion(loanNumber, currentSchedule.ScheduleVersionNo);
                foreach (var hierarchy in paymentHierarchy.Hierarchy)
                {
                    //outstanding by using hierarchy
                    if (hierarchy.Element.ToLower() == "principal") // outstanding principal
                    {
                        principalAmount = schedule.PrincipalAmount;
                    }
                    else if (hierarchy.Element.ToLower() == "interest") // outstanding interest
                    {
                        interestAmount = schedule.InterestAmount;
                    }
                    else if (hierarchy.Element.ToLower() == "additionalinterest")
                    {
                        additionalInterest = accrualDetails.Schedule?.AdditionalInterest ?? 0;
                    }
                    else
                    {
                        var fees = await accrual.GetUnPaidFees(loanNumber, currentSchedule.ScheduleVersionNo);
                        var feeToBeApplied = fees.Where(x => x.FeeName == hierarchy.Element).ToList();
                        if (feeToBeApplied != null && feeToBeApplied.Count > 0)
                        {
                            foreach (var item in feeToBeApplied)
                            {
                                var fee = new Payment.Processor.FeeDetails();
                                fee.Id = item.FeeName;
                                fee.FeeName = item.FeeName;
                                fee.FeeAmount = item.FeeAmount;
                                feeDetails.Add(fee);
                            }
                        }
                    }
                }
                if (accrualDetails.PBOC.CurrentPrincipalOutStanding > 0 || accrualDetails.PBOC.CurrentInterestOutStanding > 0)
                {
                    principalAmount = principalAmount + accrualDetails.PBOC.CurrentPrincipalOutStanding;
                    interestAmount = interestAmount + accrualDetails.PBOC.CurrentInterestOutStanding - additionalInterest;
                }
                paymentAmount = principalAmount + interestAmount + additionalInterest + feeDetails.Sum(x => x.FeeAmount);

            }

            var funding = await paymentService.GetFundingData(EntityType, loanNumber);

            if (IsAutoPay == true && loanDetails.AutoPayStartDate != null && loanDetails.AutoPayStopDate != null && (loanDetails.AutoPayStartDate.Time.Date > schedule.ScheduleDate.Date && loanDetails.AutoPayStopDate.Time.Date <= schedule.ScheduleDate.Date))
                throw new InvalidArgumentException($"Auto pay is stop for this date for loan {loanNumber}");
            if (IsAutoPay == true && loanDetails.AutoPayStopDate == null && schedule.ScheduleDate.Date < loanDetails.AutoPayStartDate.Time.Date)
                throw new InvalidArgumentException($"Auto pay is stop for this date for loan {loanNumber}");
            if (IsAutoPay == false && loanDetails.AutoPayStartDate == null && schedule.ScheduleDate.Date >= loanDetails.AutoPayStopDate.Time.Date)
                throw new InvalidArgumentException($"Auto pay is stop for this date for loan {loanNumber}");

            bool useCustomizedPaymentAmount = false;
            if (Configuration.IsSquaredOffScheduleNotPull)
            {
                var accrualDetails = await accrual.GetLoanByScheduleVersion(loanNumber, currentSchedule.ScheduleVersionNo);
                var interestOutStanding = accrualDetails != null && accrualDetails.PBOT != null ? accrualDetails.PBOT.TotalInterestOutstanding : 0;
                var scheduleTracking = await accrual.GetScheduleByLoanNumberVersion(loanNumber, currentSchedule.ScheduleVersionNo);
                var scheduleTrack = scheduleTracking != null ? scheduleTracking.ScheduleTracking.Where(x => x.ScheduleDate == schedule.ScheduleDate.Date).ToList() : null;
                if (scheduleTrack != null && scheduleTrack.Count > 0 && scheduleTrack.Any(x => x.IsPaid))
                {
                    return null;
                }
                else if (scheduleTrack != null && scheduleTrack.Count > 0)
                {
                    var lastSchedule = scheduleTrack.LastOrDefault();
                    if(interestOutStanding > lastSchedule.ExpectedInterest)
                    {
                        interestAmount = lastSchedule.ExpectedInterest - scheduleTrack.Sum(x => x.InterestAmount);
                    }
                    else
                    {
                        interestAmount = interestOutStanding;
                    }
                    principalAmount = lastSchedule.RemainingPrincipal;
                    paymentAmount = Math.Round(principalAmount + interestAmount, 2);
                    if(!Configuration.UsePaymentHierarchy)
                    {
                        interestAmount = 0;
                        principalAmount = 0;
                    }
                    useCustomizedPaymentAmount = true;
                }
            }

            IFundingRequest request = new FundingRequest();
            request.PaymentAmount = useCustomizedPaymentAmount ? Math.Round(paymentAmount, 2) : schedule.PaymentAmount < 0 ? 0 : Math.Round(schedule.PaymentAmount, 2);
            request.InterestAmount = interestAmount;
            request.PrincipalAmount = principalAmount;
            request.AdditionalInterestAmount = additionalInterest;
            request.Fees = feeDetails;
            request.BankAccountNumber = bankDetail.AccountNumber.ToString();
            request.BankAccountType = bankDetail.AccountType.ToString();
            request.BankRTN = bankDetail.RoutingNumber.ToString();
            request.BorrowersName = bankDetail.BankHolderName;
            request.PaymentScheduleDate = schedule.ScheduleDate;
            request.RequestedDate = tenantTime.Now;
            request.Status = "initiated";
            request.StatusUpdatedDate = tenantTime.Now;
            request.PaymentInstrumentType = instrument.PaymentInstrumentType;
            request.PaymentType = Payment.Processor.PaymentType.Scheduled;
            request.PaymentPurpose = Configuration.UsePaymentHierarchy ? Configuration.CustomPurpose : Configuration.SchedulePurpose;
            request.PaymentInstallmentNumber = schedule.Installmentnumber;
            request.PaymentRail = PaymentRail.ACH;
            request.TransactionType = Payment.Processor.TransactionType.Debit;
            request.PaymentAccountDate = schedule.OriginalScheduleDate == DateTime.MinValue || schedule.OriginalScheduleDate == null ? schedule.ScheduleDate : schedule.OriginalScheduleDate;

            IFunding fundingData = null;
            if (request.PaymentAmount > 0)
            {
                fundingData = await paymentService.AddFundingRequest(EntityType, loanNumber, request);
                schedule.IsProcessed = true;
                await loanOnboardingService.UpdatePaidDetails(loanNumber, currentSchedule);
                await eventHub.Publish(new PullRequestCreated
                {
                    EntityId = loanNumber,
                    EntityType = EntityType,
                    Response = fundingData
                });
            }
            return fundingData;
        }
        
        public async Task<ProductDetails> GetProductDetail(string loanNumber, IDataAttributesEngine dataAttributesEngine)
        {
            var productDetails = await dataAttributesEngine.GetAttribute("loan", loanNumber, "product");
            var productObject = JsonConvert.DeserializeObject<List<ProductDetails>>(productDetails.ToString());
            return productObject.FirstOrDefault();
        }

    }
}
