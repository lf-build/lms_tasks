﻿using LendFoundry.Calendar.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;

#endif
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LMS.Payment.Processor.Client;
using LMS.LoanManagement.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Logging;
using LMS.Loan.Filters.Client;
using LMS.LoanAccounting;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.CreatePullRequest
{
    public class PaymentPullAgentFactory : IPaymentPullAgentFactory
    {


        public PaymentPullAgentFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var loanFilterFactory = Provider.GetService<ILoanFilterClientServiceFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loanManagementClientServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var accrual = Provider.GetService<IAccrualBalanceClientFactory>();
            var productServiceFactory = Provider.GetService<IProductServiceFactory>();
            var controlFileFactory = Provider.GetService<IControlFileFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory,
            loanFilterFactory, loanManagementClientServiceFactory, eventHubClientFactory,
             calendarServiceFactory, paymentServiceClientFactory, dataAttributesClientFactory, accrual,
              loggerFactory, tenantServiceFactory, productServiceFactory, controlFileFactory);
        }
    }
}
