﻿using LMS.Payment.Processor;

namespace LMS.Tasks.Loan.CreatePullRequest
{
    public class PullRequestCreated
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public IFunding Response { get; set; }
     
    }
}
