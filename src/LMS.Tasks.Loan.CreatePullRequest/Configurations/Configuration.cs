﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace LMS.Tasks.Loan.CreatePullRequest
{
    public class Configuration : IDependencyConfiguration
    {
        public int NotificationSendDays { get; set; }
        public List<string> LoanStatus { get; set; }
        public string SchedulePurpose { get; set; }
        public string CustomPurpose { get; set; }
        public string PaymentHierarchyName { get; set; }
        public bool UsePaymentHierarchy { get; set; }
        public bool IsSquaredOffScheduleNotPull { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
