﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.CreatePullRequest
{
    public interface IPaymentPullAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}