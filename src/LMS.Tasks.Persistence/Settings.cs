using LendFoundry.Foundation.Services.Settings;
using System;

namespace LMS.Tasks
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONTROL_FILE_CONFIGURATION_NAME") ?? "control-file";
    }
}