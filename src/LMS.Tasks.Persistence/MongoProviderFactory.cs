using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LMS.Tasks
{
    public class MongoProviderFactory : IControlFileFactory
    {

        public MongoProviderFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
            
        public IControlFile Create(ITokenReader reader)
        {
            var taskRepositoryFactory = Provider.GetService<ITaskRepositoryFactory>();
            var taskRepository = taskRepositoryFactory.Create(reader);

            return new MongoProvider(taskRepository);
        }
    }
}

