﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LMS.Tasks;
using LendFoundry.Security.Encryption;
using LendFoundry.Foundation.Logging;

namespace LMS.Tasks
{
    public class TaskRepositoryFactory : ITaskRepositoryFactory
    {
        #region Constructor
        public TaskRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }
        #endregion

        #region Public Methods
        public ITaskRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new TaskRepository(tenantService, configuration);
        }
        #endregion
    }
}
