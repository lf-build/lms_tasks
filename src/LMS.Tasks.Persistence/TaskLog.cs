﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Tasks
{
    public class TaskLog : Aggregate, ITaskLog
    {
        public string TaskName { get; set; }
        public TimeBucket StartDate { get; set; }
        public object TaskLogs { get; set; }
    }
}
