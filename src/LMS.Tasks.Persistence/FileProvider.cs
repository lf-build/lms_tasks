using LendFoundry.Configuration;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks;
using Renci.SshNet;
using RestSharp.Serializers;
using System;
using System.IO;
using System.Text;

namespace LMS.Tasks
{
    public class FileProvider : IControlFile
    {
        public FileProvider(IConfigurationServiceFactory configurationFactory, ITokenHandler tokenHandler)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITokenHandler TokenHandler { get; }

        public void AddControlFile(string taskName, DateTimeOffset startTime, object Obj, string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                throw new InvalidOperationException("Configuration service not found for the SFTP configuration  while creating control files");
            }
            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                throw new InvalidOperationException("Configuration not found for the SFTP configuration  while creating control files");
            }
            if (Obj == null)
            {
                throw new InvalidOperationException("Object value could not be null for control file");
            }
            string jsondata = new JsonSerializer().Serialize(Obj);

            using (var stream = new MemoryStream(Encoding.ASCII.GetBytes(jsondata)))
            {
                using (var client = GetSftpClient(Configuration))
                {
                    if (client == null)
                    {
                        throw new InvalidOperationException("Sftp Client is not initialized");
                    }

                    FaultRetry.RunWithAlwaysRetry(client.Connect);
                    var destination = Path.Combine(Configuration.FilePath, $"{taskName}_{startTime.ToString("yyyyMMdd")}.json");
                    FaultRetry.RunWithAlwaysRetry(
                        () => client.UploadFile(stream, destination, true));
                }
            }
        }
        private SftpClient GetSftpClient(Configuration configuration)
        {
            return new SftpClient(configuration.Host , configuration.Port,
               configuration.Username, configuration.Password);
        }
    }
}
