using LendFoundry.Foundation.Date;
using Microsoft.CSharp.RuntimeBinder;
using System;

namespace LMS.Tasks
{
    public class MongoProvider :IControlFile
    {
        public MongoProvider(ITaskRepository taskRepository)
        {
            TaskRepository = taskRepository;
        }

        private ITaskRepository TaskRepository { get; }

        public void AddControlFile(string taskName, DateTimeOffset startTime, object Obj, string tenant)
        {
            var logs = new TaskLog();
            logs.TaskName = taskName;
            logs.StartDate = new TimeBucket(startTime);
            logs.TaskLogs = Obj;
            TaskRepository.Add(logs);
        }

        private static string GetValueProperty(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.p_grade;
            return HasProperty(resultProperty) ? GetValue(resultProperty) : null;
        }
        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }
        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }
    }
}
