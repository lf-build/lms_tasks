﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Tasks
{
    public interface IControlFileFactory
    {
        IControlFile Create(ITokenReader reader);
    }
}
