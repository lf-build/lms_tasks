﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LMS.Tasks
{
    public interface ITaskLog : IAggregate
    {
         string TaskName { get; set; }
         TimeBucket StartDate { get; set; }
         object TaskLogs { get; set; }
    }
}