﻿
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Tasks
{
    public interface ITaskRepository : IRepository<ITaskLog>
    {
        Task<List<ITaskLog>> GetByTaskName(TaskLoanRequest request);
        Task<List<ITaskLog>> GetByStartDate(TaskLoanRequest request);
        Task<List<ITaskLog>> GetAll();
    }
}
