﻿using System;
using System.Collections.Generic;

namespace LMS.Tasks
{
    public class TaskLoanRequest
    {
        public List<string> TaskName { get; set; }
        public DateTimeOffset FromDate { get; set; }
        public DateTimeOffset ToDate { get; set; }
    }
}
