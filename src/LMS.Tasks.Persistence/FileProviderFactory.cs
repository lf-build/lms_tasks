using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Configuration;

namespace LMS.Tasks
{
    public class FileProviderFactory : IControlFileFactory
    {

        public FileProviderFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
            
        public IControlFile Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            return new FileProvider(configurationServiceFactory , tokenFactory);
        }
    }
}

