﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Tasks
{
   public interface IControlFile
    {
        void AddControlFile(string taskName, DateTimeOffset startTime, object Obj, string tenant);
    }
}
