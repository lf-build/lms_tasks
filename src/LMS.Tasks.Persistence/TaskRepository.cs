﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Date;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks
{
    public class TaskRepository : MongoRepository<ITaskLog, TaskLog>, ITaskRepository
    {
        static TaskRepository()
        {
            BsonClassMap.RegisterClassMap<TaskLog>(map =>
            {
                map.AutoMap();               
                var type = typeof(TaskLog);
                map.MapProperty(s => s.TaskLogs).SetSerializer(new BsonJsonSerializer<object>());
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.MapProperty(s => s.TaskLogs).SetSerializer(new BsonJsonSerializer<object>());
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });           

        }

        public TaskRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "task-logs")
        {
            CreateIndexIfNotExists("ref-id", Builders<ITaskLog>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.TaskName));

        }
        public async Task<List<ITaskLog>> GetByTaskName(TaskLoanRequest request)
        {
            return await Query.Where(c => request.TaskName.Contains(c.TaskName)).ToListAsync();
        }

        public async Task<List<ITaskLog>> GetByStartDate(TaskLoanRequest request)
        {
            return await Query.Where(c => c.StartDate.Time >= request.FromDate.Date && c.StartDate.Time <= request.ToDate).ToListAsync();
        }

        public async Task<List<ITaskLog>> GetAll()
        {
            return await Query.ToListAsync();
        }
    }
}