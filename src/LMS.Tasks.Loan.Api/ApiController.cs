﻿using LendFoundry.Configuration;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.Framework.DependencyInjection;

#endif
using System;
using System.Threading.Tasks;
using LendFoundry.Security.Tokens;
using LMS.Tasks.Loan.CreateInstruction;
using LMS.Payment.Processor.Client;
using LMS.LoanManagement.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using System.Linq;
using LMS.Tasks.Loan.PaymentSuccess;
using LMS.LoanAccounting;
using LMS.Loan.Filters.Client;
using LMS.Tasks.Loan.CreateFeePullRequest;
using LMS.Tasks.Loan.Funding;
using LMS.Tasks.Loan.NSFRetry;
using LendFoundry.StatusManagement.Client;
using System.Collections.Generic;
using LMS.Tasks.Loan.CreatePullRequest;
using LendFoundry.InstantBankVerification;
using LendFoundry.EventHub;
using LMS.Payment.Processor;
using LendFoundry.Foundation.Logging;
using LMS.Loan.Filters.Abstractions;
using LendFoundry.DataAttributes;
using LendFoundry.StatusManagement;

namespace LMS.Tasks.Loan.Api
{
    /// <summary>
    /// class ApiController
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// ApiController
        /// </summary>
        /// <param name="provider"></param>
        public ApiController(IServiceProvider provider)
        {
            Provider = provider;

            var instructionService = provider.GetService<IInstructionAgentFactory>();
            var paymentService = provider.GetService<IPaymentAgentFactory>();
            var feeService = provider.GetService<IFeeAgentFactory>();
            var refundService = provider.GetService<IFundingAgentFactory>();
            var nsfService = provider.GetService<INSFRetryAgentFactory>();
            var paymentPullServiceFactory = provider.GetService<IPaymentPullAgentFactory>();
            TokenReader = provider.GetService<ITokenReader>();
            ConfigurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var loanManagementClientServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loanFilterClientServiceFactory = Provider.GetService<ILoanFilterClientServiceFactory>();
            var accrualBalanceClientFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory>();
            var achServiceFactory = Provider.GetService<IAchServiceFactory>();
            var instantBankVerificationClientFactory = Provider.GetService<IInstantBankVerificationClientFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();

            TenantTime = tenantTimeFactory.Create(ConfigurationServiceFactory, TokenReader);
            EventHubClient = eventHubClientFactory.Create(TokenReader);
            LoanManagementClientService = loanManagementClientServiceFactory.Create(TokenReader);
            PaymentProcessorService = paymentServiceClientFactory.Create(TokenReader);
            LoanFilterService = loanFilterClientServiceFactory.Create(TokenReader);
            AccrualBalanceService = accrualBalanceClientFactory.Create(TokenReader);
            DataAttributesEngine = dataAttributesClientFactory.Create(TokenReader);
            CalendarService = calendarServiceFactory.Create(TokenReader);
            AchService = achServiceFactory.Create(TokenReader);
            InstantBankVerificationService = instantBankVerificationClientFactory.Create(TokenReader);
            InstructionService = instructionService.Create(TokenReader);
            PaymentService = paymentService.Create(TokenReader);
            FeeService = feeService.Create(TokenReader);
            RefundService = refundService.Create(TokenReader);
            NSFService = nsfService.Create(TokenReader);
            PaymentPullService = paymentPullServiceFactory.Create(TokenReader);

        }


        /// <summary>
        /// Provider
        /// </summary>
        /// <value></value>
        public IServiceProvider Provider { get; }
        private CreateInstruction.Agent InstructionService { get; set; }
        private PaymentSuccess.Agent PaymentService { get; set; }
        private CreateFeePullRequest.Agent FeeService { get; set; }
        private Funding.Agent RefundService { get; set; }
        private NSFRetry.Agent NSFService { get; set; }
        private CreatePullRequest.Agent PaymentPullService { get; set; }
        private ITokenReader TokenReader { get; set; }
        private ITenantTime TenantTime { get; set; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; set; } 
        private IEventHubClient EventHubClient { get; set; }
        private ILoanManagementClientService LoanManagementClientService { get; set; }
        private IPaymentProcessorService PaymentProcessorService { get; set; }
        private ILoanFilterService LoanFilterService { get; set; }
        private IAccrualBalanceService AccrualBalanceService { get; set; }
        private IDataAttributesEngine DataAttributesEngine { get; set; }
        private ICalendarService CalendarService  { get; set; }
        private IAchService AchService { get; set; }
        private IInstantBankVerificationService InstantBankVerificationService { get; set; }
        
        /// <summary>
        /// InitiatePayment
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="schduleDate"></param>
        /// <returns></returns>
        [HttpPost("/{loanNumber}/{schduleDate}/initiate/payment")]
        public async Task<IActionResult> InitiatePayment(string loanNumber, string schduleDate)
        {
            try
            {
                var configuration = ConfigurationServiceFactory.Create<CreatePullRequest.Configuration>("task-create-payment-pull", TokenReader).Get();
                
                var paymentDate = Convert.ToDateTime(schduleDate);
                var loan = await LoanManagementClientService.GetLoanInformationByLoanNumber(loanNumber);
                var currentSchedule = await LoanManagementClientService.GetLoanSchedule(loanNumber);
                var scheduleToBeProcess = currentSchedule.ScheduleDetails.Where(x => x.IsProcessed == false && x.IsMissed != true && x.ScheduleDate.Date == paymentDate.Date).ToList();
                if (scheduleToBeProcess == null)
                    throw new NotFoundException($"No Payment schedule found for loan {loanNumber} on date {schduleDate}");
                var paymentHierarchyName = !string.IsNullOrWhiteSpace(configuration.PaymentHierarchyName) ? configuration.PaymentHierarchyName : "default";
                var result = new List<string>();
                foreach (var item in scheduleToBeProcess)
                {
                    var response = await PaymentPullService.PaymentPull(LoanManagementClientService, PaymentProcessorService, TenantTime, EventHubClient, DataAttributesEngine, AccrualBalanceService, configuration, loanNumber, loan.IsAutoPay, currentSchedule, item, paymentHierarchyName);
                    if (response != null)
                    {
                        result.Add(response.ReferenceId);
                    }
                }
                return await ExecuteAsync(async () => await Task.Run(() => Ok(result)));
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }


        /// <summary>
        /// AddFundingRequest
        /// </summary>
        /// <param name="referenceId"></param>
        /// <returns></returns>
        [HttpGet("/{referenceId}/instruction")]
        public Task<IActionResult> AddFundingRequest(string referenceId)
        {
            return ExecuteAsync(async () =>
                {
                    try
                    {
                        var configuration = ConfigurationServiceFactory.Create<CreateInstruction.Configuration>("task-create-instruction", TokenReader).Get();

                        var fundingData = await PaymentProcessorService.GetFundingByReferenceId(referenceId);
                        if (fundingData == null)
                        {
                            throw new NotFoundException($"No funding data found for referenceId {referenceId}");
                        }
                        IList<IFunding> fundings = new List<IFunding>();
                        fundings.Add(fundingData);

                        return Ok(await InstructionService.CreateInstructions(fundings, PaymentProcessorService, TenantTime, AchService, EventHubClient, LoanManagementClientService, InstantBankVerificationService, DataAttributesEngine, CalendarService, configuration, Logger));

                    }
                    catch (Exception ex)
                    {
                        return ErrorResult.BadRequest(ex.Message);
                    }
                });
        }

        /// <summary>
        /// MakePayment
        /// </summary>
        /// <param name="referenceId"></param>
        /// <param name="SchduleDate"></param>
        /// <returns></returns>
        [HttpGet("/{referenceId}/{SchduleDate}/payment")]
        public Task<IActionResult> MakePayment(string referenceId, string SchduleDate)
        {
            return ExecuteAsync(async () =>
                {
                    try
                    {
                        var fundingData = await PaymentProcessorService.GetFundingByReferenceId(referenceId);

                        if (fundingData == null || !fundingData.IsFileCreated || fundingData.IsAccounted)
                        {
                            throw new NotFoundException($"No funding data found for referenceId {referenceId}, IsFileCreated {fundingData.IsFileCreated} and IsAccounted {!fundingData.IsAccounted}");
                        }
                        var scheduleDate = Convert.ToDateTime(SchduleDate);
                        return Ok(await (PaymentService.MakePayment(PaymentProcessorService, TenantTime, AccrualBalanceService, EventHubClient, fundingData, scheduleDate.Date)));
                    }
                    catch (Exception ex)
                    {
                        return ErrorResult.BadRequest(ex.Message);
                    }
                });

        }

        /// <summary>
        ///  InitiateFee
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="SchduleDate"></param>
        /// <returns></returns>
        [HttpPost("/{loanNumber}/{SchduleDate}/initiate/fee")]
        public Task<IActionResult> InitiateFee(string loanNumber, string SchduleDate)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    var configuration = ConfigurationServiceFactory.Create<CreateFeePullRequest.Configuration>("task-create-fee-pull", TokenReader).Get();

                    var feeScheduleDate = Convert.ToDateTime(SchduleDate);
                    var loan = await LoanManagementClientService.GetLoanInformationByLoanNumber(loanNumber);

                    var currentSchedule = await LoanManagementClientService.GetLoanSchedule(loanNumber);
                    var scheduleToBeProcess = currentSchedule.FeeDetails.Where(x => x.FeeDueDate.Date == feeScheduleDate.Date && x.FeeType == "Scheduled" && x.IsProcessed == false).FirstOrDefault();
                    if (scheduleToBeProcess == null)
                    {
                        throw new NotFoundException($"No fee schedule found for loan {loanNumber} on date {SchduleDate}");
                    }

                    return Ok(await (FeeService.InitiateFeePullRequest(LoanManagementClientService, PaymentProcessorService, TenantTime, EventHubClient, configuration, scheduleToBeProcess, AccrualBalanceService, loan)));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        /// <summary>
        /// MakeRefundPayment
        /// </summary>
        /// <param name="referenceId"></param>
        /// <returns></returns>
        [HttpGet("/{referenceId}/refund")]
        public Task<IActionResult> MakeRefundPayment(string referenceId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    var configuration = ConfigurationServiceFactory.Create<Funding.Configuration>("task-funding-payment", TokenReader).Get();

                    var fundingData = await PaymentProcessorService.GetFundingRequest(configuration.FundingStatus);
                    var attemptsToBeMade = fundingData.Where(x => x.PaymentType == Payment.Processor.PaymentType.Refund && x.ReferenceId == referenceId).FirstOrDefault();
                    if (attemptsToBeMade == null)
                    {
                        throw new NotFoundException($"No funding data found for referenceId {referenceId}");
                    }
                    await RefundService.MakeFundingRequest(PaymentProcessorService, TenantTime, EventHubClient, AchService, AccrualBalanceService, configuration, attemptsToBeMade);
                    return new NoContentResult();
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        /// <summary>
        /// MakeNSFPayment
        /// </summary>
        /// <param name="referenceId"></param>
        /// <returns></returns>
        [HttpGet("/{referenceId}/nsf/retry")]
        public Task<IActionResult> MakeNSFPayment(string referenceId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    var configuration = ConfigurationServiceFactory.Create<NSFRetry.Configuration>("task-nsf-retry", TokenReader).Get();
                    var failPayments = await PaymentProcessorService.GetFundingRequest(new List<string>() { "failed" });
                    var failPaymentToBeProcess = failPayments.Where(x => x.ReferenceId == referenceId && x.RetryCount < configuration.MaxRetryCount).FirstOrDefault();

                    if (failPaymentToBeProcess == null)
                        throw new NotFoundException($"No funding data found for referenceId {referenceId}");

                    await NSFService.MakeNSFPayment(PaymentProcessorService, failPaymentToBeProcess);
                    return new NoContentResult();
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }


        /// <summary>
        /// GetByTaskName
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/by/name")]
        public Task<IActionResult> GetByTaskName([FromBody]TaskLoanRequest request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    if (request.TaskName == null && request.TaskName.Count == 0)
                        throw new NotFoundException($"task name can not be null");

                    var taskRepositoryFactory = Provider.GetService<ITaskRepositoryFactory>();
                    var taskRepository = taskRepositoryFactory.Create(TokenReader);
                    return Ok(await (taskRepository.GetByTaskName(request)));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetByStartDate
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/by/startdate")]
        public Task<IActionResult> GetByStartDate([FromBody]TaskLoanRequest request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    if (request.FromDate == null || request.ToDate == null)
                        throw new NotFoundException($" {request.FromDate} and {request.ToDate} can not be null");
                    if (request.FromDate > request.ToDate)
                        throw new NotFoundException($" {request.FromDate} can not be greater than {request.ToDate}");
                    if (request.FromDate == request.ToDate)
                        request.ToDate = request.ToDate.AddDays(1).Date;

                    var taskRepositoryFactory = Provider.GetService<ITaskRepositoryFactory>();
                    var taskRepository = taskRepositoryFactory.Create(TokenReader);
                    return Ok(await (taskRepository.GetByStartDate(request)));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        [HttpGet("/all")]
        public Task<IActionResult> GetAll()
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    var taskRepositoryFactory = Provider.GetService<ITaskRepositoryFactory>();
                    var taskRepository = taskRepositoryFactory.Create(TokenReader);
                    return Ok(await (taskRepository.GetAll()));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
