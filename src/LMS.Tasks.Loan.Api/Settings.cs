﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LMS.Tasks.Loan.api
{
    /// <summary>
    /// Settings
    /// </summary>
    public static class Settings
    {
         /// <summary>
         /// ServiceName
         /// </summary>
         /// <returns></returns>
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-api";
       
    }
}