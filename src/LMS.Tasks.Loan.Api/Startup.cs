﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.Loan.Filters.Client;
using LMS.LoanAccounting.Client;
using LMS.LoanManagement.Client;
using LMS.Payment.Processor.Client;
using LMS.Tasks.Loan.CreateFeePullRequest;
using LMS.Tasks.Loan.CreateInstruction;
using LMS.Tasks.Loan.PaymentSuccess;
using LMS.Tasks.Loan.Funding;
using LMS.Tasks.Loan.NSFRetry;
using LendFoundry.StatusManagement.Client;
using LMS.Tasks.Loan.CreatePullRequest;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.InstantBankVerification.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LMS.Foundation.Amortization;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;

#endif

namespace LMS.Tasks.Loan.Api
{

  /// <summary>
  /// Startup class
  /// </summary>
  public class Startup
      {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
        }

        /// <summary>
        /// ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Task Api"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LMS.Tasks.Loan.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<LMS.Tasks.Loan.CreateFeePullRequest.Configuration>(Settings.ServiceName);
            services.AddConfigurationService<LMS.Tasks.Loan.CreateFeePullRequest.Configuration>(Settings.ServiceName);

            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddDataAttributes();
            services.AddAchService();
            services.AddIBVService();
            services.AddCalendarService();
            services.AddPaymentProcessorService();
            services.AddLoanManagementService();
            services.AddAmortization();
            services.AddLoanFilterService();
            services.AddAccrualBalance();
            services.AddStatusManagementService();

            services.AddTransient<IInstructionAgentFactory, InstructionAgentFactory>();
            services.AddTransient<IPaymentAgentFactory, PaymentAgentFactory>();
            services.AddTransient<IFeeAgentFactory, FeeAgentFactory>();
            services.AddTransient<INSFRetryAgentFactory, NSFRetryAgentFactory>();
            services.AddTransient<IPaymentPullAgentFactory, PaymentPullAgentFactory>();
            services.AddTransient<IFundingAgentFactory, FundingAgentFactory>();
            services.AddTransient<ITaskRepository, TaskRepository>();
            services.AddTransient<IControlFileFactory, MongoProviderFactory>();

            //   services.AddTransient<IControlFile, MongoProvider>();
            services.AddTransient<ITaskRepositoryFactory, TaskRepositoryFactory>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
    		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Task api");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();

        }
    }
}
