﻿#if DOTNET2

using System;
using Microsoft.AspNetCore.Hosting;

namespace LMS.Tasks.Loan.Api
{
    /// <summary>
    /// Program
    /// </summary>
    public class Program
    {
        /// <summary>
        ///  Main Method
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }

        }


    }
}

#endif