using System;
using System.IO;
using System.Linq;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductConfiguration;
using LendFoundry.Security.Tokens;
using LMS.Loan.Filters.Client;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.LoanManagement.Client;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.CheckRenewal
{
    public class Agent : ScheduledAgent
    {
        public Agent
            (
                IConfigurationServiceFactory configurationFactory,
                ITokenHandler tokenHandler,
                ITenantTimeFactory tenantTimeFactory,
                IAccrualBalanceClientFactory accrualServiceFactory,
                ILoggerFactory loggerFactory,
                ITenantServiceFactory tenantServiceFactory,
                ILoanFilterClientServiceFactory loanFilterClientServiceFactory,
                ILoanManagementClientServiceFactory loanManagementClientService,
                IEventHubClientFactory eventHubFactory,
                IDecisionEngineClientFactory decisionEngineService
            ) : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            AccrualServiceFactory = accrualServiceFactory;
            LoggerFactory = loggerFactory;
            LoanFilterClientServiceFactory = loanFilterClientServiceFactory;
            LoanManagementClientService = loanManagementClientService;
            DecisionEngineService = decisionEngineService;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IAccrualBalanceClientFactory AccrualServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IDecisionEngineClientFactory DecisionEngineService { get; }
        private ILoanFilterClientServiceFactory LoanFilterClientServiceFactory { get; }
        private ILoanManagementClientServiceFactory LoanManagementClientService { get; set; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        
        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var eventHub = EventHubFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var accrual = AccrualServiceFactory.Create(reader);
            var loanManagement = LoanManagementClientService.Create(reader);
            var loanFilterService = LoanFilterClientServiceFactory.Create(reader);
            var decisionEngine = DecisionEngineService.Create(reader);

            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the Configuration related to renewal");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Was not found the Configuration related to renewal");
                return;
            }
            try
            {
                logger.Info($"Started Running the task for renewal");
                var loans =await loanFilterService.GetAllByStatus(Configuration.LoanStatus);
                loans = loans.Where(x => Configuration.LoanProductId.Contains(x.LoanProductId)).ToList();

                foreach (var loan in loans)
                {
                    try
                    {
                        var loanData =await loanManagement.GetLoanInformationByLoanNumber(loan.LoanNumber);
                        if (loanData == null)
                            throw new NotFoundException($"LoanData not found for Loan : {loan.LoanNumber }");
                        if (loanData.IsRenewable == "false")
                        {
                            var currentSchedule =await loanManagement.GetLoanSchedule(loan.LoanNumber);
                            if (currentSchedule == null)
                                throw new NotFoundException($"Schedule Detail not found  for Loan : {loan.LoanNumber}");

                            var accrulaDetails =await accrual.GetLoanByScheduleVersion(loan.LoanNumber, currentSchedule.ScheduleVersionNo);
                            if (accrulaDetails == null)
                                throw new NotFoundException($"Accrual Data not found for Loan : {loan.LoanNumber }");

                            var input = new { LoanData = loanData, ScheduleDetails = currentSchedule, AccualDetails = accrulaDetails };
                            var executionResult = decisionEngine.Execute<dynamic, ResultDetail>(
                                    Configuration.RuleName, new { payload = input });
                            ILoanInformation loanDetails = new LoanInformation();
                            if (executionResult.Result == true)
                            {
                                loanDetails =await loanManagement.UpdateRenewalDetails(loan.LoanNumber, new UpdateRenewalDetailRequest() { IsRenewal = "true" });
                                await eventHub.Publish("RenewalEligible", new
                                {
                                    EntityId = loan.LoanNumber,
                                    EntityType = "loan",
                                    LoanDetails = loanDetails
                                });
                            }
                            else if (executionResult.Result == false && !string.IsNullOrWhiteSpace(executionResult.ErrorDetail))
                            {
                                logger.Error($"Error while executing the rule for loan : {loan.LoanNumber } Error : {executionResult.ErrorDetail}");
                            }
                        }

                    }
                    catch (System.Exception e)
                    {
                        logger.Error($"Error While Renewal for Loan : {loan.LoanNumber }", e);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error while Running the task for renewal", e);
            }

        }
    }
}