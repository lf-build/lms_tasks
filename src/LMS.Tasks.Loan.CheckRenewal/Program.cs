﻿using LendFoundry.Configuration.Client;
using LMS.Loan.Filters.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.LoanAccounting.Client;
using LMS.LoanManagement.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tasks;
using LendFoundry.Foundation.ServiceDependencyResolver;
namespace LMS.Tasks.Loan.CheckRenewal
{
    public class Program : LendFoundry.Tasks.DependencyInjection
    {
        public static void Main(string[] args)
        {

            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();

        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddLoanFilterService();
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddAccrualBalance();
            services.AddLoanManagementService();
            services.AddEventHub(Settings.ServiceName);
            services.AddDecisionEngine();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<ITaskRepositoryFactory, TaskRepositoryFactory>();
            services.AddTransient<IControlFileFactory, MongoProviderFactory>();
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName); 
            services.AddTransient<IAgent, Agent>();
            return services;
        }
    }
}
