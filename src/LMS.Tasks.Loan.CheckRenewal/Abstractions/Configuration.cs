using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LMS.Tasks.Loan.CheckRenewal
{
    public class Configuration :IDependencyConfiguration 
     {
        public List<string> LoanStatus { get; set; }        
        public List<string> LoanProductId { get; set; }
        public string RuleName{ get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}