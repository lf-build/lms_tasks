using System.Collections.Generic;

namespace LMS.Tasks.Loan.CheckRenewal
{
    public class ResultDetail   {
        public bool Result {get;set;}        
        public string ErrorDetail{ get; set; }
    }
}