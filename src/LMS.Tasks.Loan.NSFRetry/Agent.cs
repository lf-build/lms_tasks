using LendFoundry.StatusManagement.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Client;
using LendFoundry.EventHub.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.NSFRetry
{
    public class Agent : ScheduledAgent
    {

        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IStatusManagementServiceFactory statusManagementFactory,
            IEventHubClientFactory eventHubFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
             IControlFileFactory controlFileFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
            EventHubFactory = eventHubFactory;
            StatusManagementFactory = statusManagementFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IStatusManagementServiceFactory StatusManagementFactory { get; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }
        private IControlFileFactory ControlFileFactory { get; }
        private static string EntityType = "loan";
        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var statusManagementService = StatusManagementFactory.Create(reader);
            var paymentService = PaymentServiceClientFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventHub = EventHubFactory.Create(reader);
            var contolFile = ControlFileFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the configuration related to nsf retry task");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Was not found the configuration related to nsf retry task");
                return;
            }
            try
            {
                var controlFileObject = new ControlDetail();
                controlFileObject.TaskName = Settings.ServiceName;
                controlFileObject.StartTime = tenantTime.Today;
                controlFileObject.ExceptionDetails = new List<ExceptionDetail>();
                controlFileObject.LoanProcessList = new List<string>();
                controlFileObject.LoanSkipList = new List<string>();
                controlFileObject.LoanSuccessList = new List<string>();
                logger.Info("Started retrying nsf fail payment");
                // var procrssingEndDate = new DateTimeOffset(new DateTime(2017, 10, 15));
                var failPayments = await paymentService.GetFundingRequest(Configuration.FundingStatus);
                var failPaymentToBeProcess = failPayments.Where(x => x.NextRetryDate != null && x.NextRetryDate.Time.Date <= tenantTime.Now.Date && x.RetryCount < Configuration.MaxRetryCount).ToList();
                controlFileObject.NoOfLoanProcess = failPaymentToBeProcess.Count;
                var successCount = 0;
                var skipCount = 0;
                var failCount = 0;
                foreach (var payment in failPaymentToBeProcess)
                {

                    try
                    {
                        controlFileObject.LoanProcessList.Add(payment.LoanNumber);
                        var statusInfo = await statusManagementService.GetActiveStatusWorkFlow(EntityType, payment.LoanNumber);
                        if (!Configuration.IncludedLoanStatus.Contains(statusInfo.Code))
                        {
                            skipCount++;
                            controlFileObject.LoanSkipList.Add(payment.LoanNumber);
                            continue;
                        }

                        var attempts = await paymentService.GetAttemptData(payment.ReferenceId);
                        var lastAttempt = attempts.OrderByDescending(c => c.AttemptDate.Time.DateTime).FirstOrDefault();
                        if (lastAttempt != null && !string.IsNullOrWhiteSpace(lastAttempt.ReturnCode) && lastAttempt.ReturnCode == "R01")
                        {
                            successCount++;
                            controlFileObject.LoanSuccessList.Add(payment.LoanNumber);
                            await MakeNSFPayment(paymentService, payment);
                        }
                    }
                    catch (Exception ex)
                    {
                        failCount++;
                        var exception = new ExceptionDetail();
                        exception.LoanNumber = payment.EntityId;
                        exception.Exception = ex.Message;
                        controlFileObject.ExceptionDetails.Add(exception);
                        logger.Error($"Error while retrying payment for loan {payment.EntityId}", ex);
                    }
                }
                controlFileObject.NoOfLoanSkip = skipCount;
                controlFileObject.NoOfLoanFail = failCount;
                controlFileObject.NoOfLoanSuccess = successCount;
                contolFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject,tenant);

                logger.Info("Ended retrying nsf fail payment");
            }
            catch (Exception e)
            {
                logger.Error("Error while retrying nsf fail payment", e);
            }

        }

        public async Task MakeNSFPayment(IPaymentProcessorService paymentService, IFunding payment)
        {
            payment.RequestStatus = "resubmitted";
            payment.IsAccounted = false;
            payment.IsFileCreated = false;
            payment.RetryCount = payment.RetryCount + 1;
            payment.IsReSubmitted = true;
            await Task.Run(() => paymentService.UpdateFundingData(payment));
        }

    }
}
