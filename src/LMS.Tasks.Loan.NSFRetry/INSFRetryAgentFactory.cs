﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.NSFRetry
{
    public interface INSFRetryAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}