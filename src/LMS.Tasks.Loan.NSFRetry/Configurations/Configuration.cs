﻿
using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LMS.Tasks.Loan.NSFRetry
{
    public class Configuration :IDependencyConfiguration
    {
        public List<string> FundingStatus { get; set; }

        public List<string> IncludedLoanStatus { get; set; }

        public int MaxRetryCount { get; set;}
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}
