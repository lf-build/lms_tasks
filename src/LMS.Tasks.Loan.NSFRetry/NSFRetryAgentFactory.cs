﻿using LendFoundry.Calendar.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;

#endif
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LMS.Payment.Processor.Client;
using LendFoundry.Foundation.Logging;
using LMS.LoanAccounting;
using LendFoundry.StatusManagement.Client;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.NSFRetry
{
    public class NSFRetryAgentFactory : INSFRetryAgentFactory
    {

        public NSFRetryAgentFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var statusFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var controlFileFactory = Provider.GetService<IControlFileFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory, statusFactory, eventHubClientFactory, paymentServiceClientFactory, loggerFactory, tenantServiceFactory, controlFileFactory);
        }
    }
}
