using LendFoundry.Foundation.Services.Settings;
using System;

namespace LMS.Tasks.Loan.NSFRetry
{
    public static class Settings
    {
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-nsf-retry";
        
    }
}