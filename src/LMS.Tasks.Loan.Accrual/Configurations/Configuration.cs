﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace LMS.Tasks.Loan.Accrual
{
    public class Configuration :IDependencyConfiguration
    {

        public List<string> LoanStatus { get; set; }

        public List<string> ToEmailAddresses { get; set; }

        public string EmailAddress { get; set; }

        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }

        public List<string> IncludedLoans { get; set; }
        public List<string> ExcludedLoans { get; set; }

        public AcrualFilterType AcrualFilter { get; set; }

        public DateTime? ProcessingDate { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

        

    }
}
