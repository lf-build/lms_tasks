﻿
using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.Loan.Filters.Client;
using LMS.LoanAccounting.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tasks;
using LendFoundry.Foundation.ServiceDependencyResolver;
namespace LMS.Tasks.Loan.Accrual
{
    public class Program : LendFoundry.Tasks.DependencyInjection
    {
        public static void Main(string[] args)
        {
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName); 
            services.AddLoanFilterService();
            services.AddEmailService();
            services.AddAccrualBalance();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<ITaskRepositoryFactory, TaskRepositoryFactory>();
            services.AddTransient<IControlFileFactory, MongoProviderFactory>();

            services.AddTransient<IAgent, Agent>();
            return services;
        }
    }
}
