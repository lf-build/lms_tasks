using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;
using LMS.Loan.Filters.Client;
using System.Collections.Generic;
using LMS.LoanAccounting;
using LendFoundry.Email.Client;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.Accrual
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ILoanFilterClientServiceFactory loanFilterClientServiceFactory,
            IAccrualBalanceClientFactory accrualBalanceClientFactory,
            IEventHubClientFactory eventHubFactory,
            IEmailServiceFactory emailSerivceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IControlFileFactory controlFileFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            AccrualBalanceClientFactory = accrualBalanceClientFactory;
            LoanFilterClientServiceFactory = loanFilterClientServiceFactory;
            EventHubFactory = eventHubFactory;
            EmailServiceFactory = emailSerivceFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private IControlFileFactory ControlFileFactory { get; }
        private IEmailServiceFactory EmailServiceFactory { get; set; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ILoanFilterClientServiceFactory LoanFilterClientServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }


        private ILoggerFactory LoggerFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }


        private IAccrualBalanceClientFactory AccrualBalanceClientFactory { get; set; }

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var accrualBalanceService = AccrualBalanceClientFactory.Create(reader);
            var emailService = EmailServiceFactory.Create(reader);
            var loanFilterService = LoanFilterClientServiceFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventHub = EventHubFactory.Create(reader);
            var contolFile = ControlFileFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error($"Was not found the Configuration related to payment pull request for Tenant {tenant}");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error($"Was not found the Configuration related to payment pull request for Tenant {tenant}");
                return;
            }
            try
            {
                logger.Info($"Started Accrual Task for Tenant {tenant}");
                var controlFileObject = new ControlDetail();
                controlFileObject.TaskName = Settings.ServiceName;
                controlFileObject.StartTime = tenantTime.Today;
                controlFileObject.ExceptionDetails = new List<ExceptionDetail>();
                controlFileObject.LoanProcessList = new List<string>();

                var processCount = 0;
                var failCount = 0;

                var IncludedLoans = Configuration.IncludedLoans;
                var ExcludedLoans = Configuration.ExcludedLoans;

                var loans = await loanFilterService.GetAllByStatus(Configuration.LoanStatus);
                if (loans.Count() > 0)
                {
                    if (Configuration.AcrualFilter == AcrualFilterType.Include)
                    {
                        loans = loans.Where(x => IncludedLoans.Contains(x.LoanNumber)).OrderByDescending(i => i.LoanNumber).ToList();
                    }
                    else if (Configuration.AcrualFilter == AcrualFilterType.Exclude)
                    {
                        loans = loans.Where(x => !ExcludedLoans.Contains(x.LoanNumber)).OrderByDescending(i => i.LoanNumber).ToList();
                    }
                }

                var processingDate = tenantTime.Now.AddDays(-1);
                if (Configuration.ProcessingDate != null)
                {
                    processingDate = Configuration.ProcessingDate.Value;
                }

                List<string> loanNumbers = new List<string>();

                logger.Info("Date: ", new { ProcessingDate = processingDate, TenantDate = tenantTime.Now, UTCDate = tenantTime.Now.UtcDateTime });
                foreach (var loan in loans)
                {
                    try
                    {
                        IAccrualRequest accrualRequest = new AccrualRequest();
                        accrualRequest.StartDate = processingDate.Date;
                        accrualRequest.EndDate = processingDate.Date;

                        await accrualBalanceService.Accrual(loan.LoanNumber, accrualRequest);
                        processCount++;
                        controlFileObject.LoanProcessList.Add(loan.LoanNumber);
                        logger.Info($"Success LoanNumber: {loan.LoanNumber} for Tenant {tenant}");
                    }
                    catch (Exception ex)
                    {
                        loanNumbers.Add(loan.LoanNumber);

                        failCount++;
                        var exception = new ExceptionDetail();
                        exception.LoanNumber = loan.LoanNumber;
                        exception.Exception = ex.Message;
                        controlFileObject.ExceptionDetails.Add(exception);
                        logger.Error($"Error while Processing Request for loan : {loan.LoanNumber}for Tenant {tenant}", ex);
                    }
                }
                //TODO
                //await emailService.Send(Configuration.TemplateName, Configuration.TemplateVersion, new
                //{
                //    loanNumbers
                //});
                controlFileObject.NoOfLoanProcess = processCount;
                controlFileObject.NoOfLoanFail = failCount;
                contolFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject, tenant);
                logger.Info($"Ended Accrual Task for Tenant {tenant}");
                logger.Info($"These are the loans for which accrual didn't happened today: " + string.Join(",",loanNumbers));

            }
            catch (Exception e)
            {
                logger.Error("Error while generating ACH Instructions", e);
            }

        }

    }
}
