﻿using System;
using System.Collections.Generic;

namespace LMS.Tasks.Loan.Accrual
{
    public class ControlDetail
    {
        public string TaskName { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public int NoOfLoanProcess { get; set; }    
        public List<string> LoanProcessList { get; set; }
        public int NoOfLoanFail { get; set; }
        public List<ExceptionDetail> ExceptionDetails { get; set; }
    }

    public class ExceptionDetail
    {
        public string LoanNumber { get; set; }
        public string Exception { get; set; }

    }
}
