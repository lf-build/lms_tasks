﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Tasks.Loan.Accrual
{
    public enum AcrualFilterType
    {
        None,
        Exclude,
        Include
    }
}
