using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.MoneyMovement.Ach.Configuration;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public class Configuration : IDependencyConfiguration
    {
        public string FundingSourceId { get; set; }
        public List<string> FundingStatus { get; set; }
        public string NSFCode { get; set; }
        public string NSFReturnReason { get; set; }
        public int NextRetryDays { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
        public bool CreateInstructionCSV { get; set; }
        public Provider Provider { get; set; }
        public string RepaymentFileName { get; set; }
    }
}