﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public enum BankAccountType
    {
        Savings,
        Current,
        Checking
    }
}
