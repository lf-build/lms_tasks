﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public class ControlDetail
    {
        public string TaskName { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public int NoOfSchedulePaymentProcess { get; set; }
        public int NoOfResubmittedPaymentProcess { get; set; }
        public int NoOfAdditionalPaymentProcess { get; set; }
        public int NoOfLoanSkip { get; set; }        
        public int NoOfLoanFail { get; set; }
        public int NoOfLoanSuccess { get; set; }
        public double TotalACHPulledAmount { get; set; }
        public List<ExceptionDetail> ExceptionDetails { get; set; }
        public List<string> LoanProcessList { get; set; }
        public List<string> LoanSkipList { get; set; }
        public List<string> LoanSuccessList { get; set; }
    }

    public class ExceptionDetail
    {
        public string LoanNumber { get; set; }
        public string Exception { get; set; }

    }
}
