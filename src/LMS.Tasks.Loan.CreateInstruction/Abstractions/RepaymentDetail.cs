using System;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public class RepaymentDetail
    {
        public int SrNo { get; set; }
        public DateTime Date { get; set; }
        public string ProductName { get; set; }
        public string BorrowerName { get; set; }
        public DateTime ScheduleDate { get; set; }
        public double ReceivedAmount { get; set; }
        public string ReferenceNumber { get; set; }
        public int InternalReferenceNumber { get; set; }
        public string LoanNumber { get; set; }
        public string FunderId { get; set; }
        public string AdditionalReferenceId { get; set; }
        public string ApplicationNumber { get; set; }
        public double ActualEmi { get; set; }
        public int InstallmentNumber { get; set; }
    }
}