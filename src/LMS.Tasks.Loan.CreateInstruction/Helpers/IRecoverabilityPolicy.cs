using System;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}