﻿using LendFoundry.Calendar.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.EventHub;
using LendFoundry.MoneyMovement.Ach.Client;
using LMS.Payment.Processor.Client;
using LMS.LoanManagement.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.InstantBankVerification;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public class InstructionAgentFactory : IInstructionAgentFactory
    {


        public InstructionAgentFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var achServiceFactory = Provider.GetService<IAchServiceFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loanManagementClientServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var cashflowClientFactory = Provider.GetService<IInstantBankVerificationClientFactory>();
            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var controlFileFactory = Provider.GetService<IControlFileFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory, eventHubClientFactory, achServiceFactory,
               paymentServiceClientFactory, loanManagementClientServiceFactory, cashflowClientFactory, dataAttributesClientFactory,
              calendarServiceFactory, loggerFactory, tenantServiceFactory, controlFileFactory);
        }
    }
}
