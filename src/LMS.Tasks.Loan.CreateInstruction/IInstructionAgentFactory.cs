﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public interface IInstructionAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}