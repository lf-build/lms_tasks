﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.InstantBankVerification.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.LoanManagement.Client;
using LMS.Payment.Processor.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tasks;
using LendFoundry.Foundation.ServiceDependencyResolver;
namespace LMS.Tasks.Loan.CreateInstruction
{
    public class Program : LendFoundry.Tasks.DependencyInjection
    {
        public static void Main(string[] args)
        {
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTenantTime();
            services.AddTokenHandler();
          //  services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddPaymentProcessorService();
            services.AddLoanManagementService();
            services.AddDataAttributes();
            services.AddAchService();
            services.AddIBVService();
            services.AddCalendarService();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<ITaskRepositoryFactory, TaskRepositoryFactory>();
            services.AddTransient<IControlFileFactory, MongoProviderFactory>();
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName); 

            return services;
        }
    }
}
