using LendFoundry.Calendar.Client;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.InstantBankVerification;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LMS.LoanManagement.Abstractions;
using LMS.LoanManagement.Client;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using System.Text;
using System.IO;
using Renci.SshNet;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IEventHubClientFactory eventHubFactory,
            IAchServiceFactory achServiceFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory,
            ILoanManagementClientServiceFactory loanClientServiceFactory,
            IInstantBankVerificationClientFactory ibvServiceFactory,
            IDataAttributesClientFactory dataAttributesEngine,
            ICalendarServiceFactory calendarServiceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IControlFileFactory controlFileFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            AchServiceFactory = achServiceFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
            LoanClientServiceFactory = loanClientServiceFactory;
            IbvServiceFactory = ibvServiceFactory;
            DataAttributesEngine = dataAttributesEngine;
            CalendarServiceFactory = calendarServiceFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private IControlFileFactory ControlFileFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IAchServiceFactory AchServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ILoanManagementClientServiceFactory LoanClientServiceFactory { get; set; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }
        private IInstantBankVerificationClientFactory IbvServiceFactory { get; }
        private IDataAttributesClientFactory DataAttributesEngine { get; }
        private ICalendarServiceFactory CalendarServiceFactory { get; }

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var paymentService = PaymentServiceClientFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var ach = AchServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var loanService = LoanClientServiceFactory.Create(reader);
            var ibvService = IbvServiceFactory.Create(reader);
            var dataAttributeService = DataAttributesEngine.Create(reader);
            var calendar = CalendarServiceFactory.Create(reader);
            var controlFile = ControlFileFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the Configuration related to createAchInstruction task");
                return;
            }

            var configuration = configurationService.Get();
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to createAchInstruction");
                return;
            }
            try
            {
                var fundingRequest = await paymentService.GetFundingRequest(configuration.FundingStatus);
                logger.Info("Started Creating ACH Instruction");

                var controlFileObject = await CreateInstructions(fundingRequest.ToList(), paymentService, tenantTime, ach, eventHub, loanService, ibvService, dataAttributeService, calendar, configuration, logger);

                controlFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject, tenant);
                logger.Info("Ended Creating ACH Instruction");
            }
            catch (Exception e)
            {
                logger.Error("Error while generating ACH Instructions", e);
            }
        }

        public async Task<Tuple<IFunding, RepaymentDetail>> CreateInstruction(IPaymentProcessorService paymentService, ITenantTime tenantTime, IAchService ach, IEventHubClient eventHub, ILoanManagementClientService loanService, IInstantBankVerificationService ibvService, IDataAttributesEngine dataAttributeService, ICalendarService calendar, Configuration Configuration, IFunding item, dynamic instructionDetails, int sequence)
        {
            var bankDetail = await loanService.GetBankDetails(item.LoanNumber);
            IFunding fundingData = null;
            RepaymentDetail instructions = null;
            if (bankDetail != null)
            {
                var productDetails = await GetProductParameters(item.LoanNumber, dataAttributeService);
                if (productDetails == null)
                    throw new ArgumentException($"Product details not set for loan number: {item.LoanNumber}");
                if (productDetails.ProductParameters == null)
                    throw new ArgumentException($"Product parameters not set for loan number: {item.LoanNumber}");

                var productParameter = productDetails.ProductParameters.ToList();
                var checkBalanceParameter = productParameter.Where(p => p.ParameterId == "CheckBalanceBeforeAutoPay").Select(r => r.Value).FirstOrDefault();
                if (checkBalanceParameter == null)
                    throw new ArgumentException($"Product parameters CheckBalanceBeforeAutoPay not set for loan number: {item.LoanNumber}");

                var staleDaysParameter = 0;
                if (checkBalanceParameter == "true")
                    staleDaysParameter = Convert.ToInt16(productParameter.Where(p => p.ParameterId == "ACCOUNT_BALANCE_STALE_DAYS").Select(r => r.Value).FirstOrDefault());

                var applicationDetails = await loanService.GetLoanInformationByLoanNumber(item.LoanNumber);

                if (!string.IsNullOrWhiteSpace(bankDetail.BankId) && checkBalanceParameter == "true")
                {
                    var cashFlowAccountDetails = await ibvService.GetBankAccountDetails("application", applicationDetails.LoanApplicationNumber, bankDetail.BankId);
                    if (cashFlowAccountDetails == null)
                        throw new ArgumentException($"CashFlow Data not found for the : {item.LoanNumber} and bankId {bankDetail.BankId} ");

                    var staleDays = (tenantTime.Now.Date - cashFlowAccountDetails.UpdatedDate.Time.Date).TotalDays;
                    if (staleDays <= staleDaysParameter)
                    {
                        if (cashFlowAccountDetails.CurrentBalance >= item.PaymentAmount)
                        {
                            var details = await AddInstruction(paymentService, tenantTime, eventHub, ach, Configuration, item, applicationDetails, loanService, productDetails, sequence);
                            fundingData = details.Item1;
                            instructions = details.Item2;
                        }
                        else
                        {
                            fundingData = await FailAttempt(paymentService, tenantTime, eventHub, calendar, Configuration, item);
                        }
                    }
                    else
                    {
                        var details = await AddInstruction(paymentService, tenantTime, eventHub, ach, Configuration, item, applicationDetails, loanService, productDetails, sequence);
                        fundingData = details.Item1;
                        instructions = details.Item2;
                    }
                }
                else
                {
                    var details = await AddInstruction(paymentService, tenantTime, eventHub, ach, Configuration, item, applicationDetails, loanService, productDetails, sequence);
                    fundingData = details.Item1;
                    instructions = details.Item2;
                }
            }
            return Tuple.Create(fundingData, instructions);
        }

        public async Task<ControlDetail> CreateInstructions(IList<IFunding> fundingRequest, IPaymentProcessorService paymentService, ITenantTime tenantTime, IAchService ach, IEventHubClient eventHub, ILoanManagementClientService loanService, IInstantBankVerificationService ibvService, IDataAttributesEngine dataAttributeService, ICalendarService calendar, Configuration Configuration, ILogger logger)
        {
            var controlFileObject = new ControlDetail();
            controlFileObject.TaskName = Settings.ServiceName;
            controlFileObject.StartTime = tenantTime.Today;
            controlFileObject.ExceptionDetails = new List<ExceptionDetail>();
            controlFileObject.LoanProcessList = new List<string>();
            controlFileObject.LoanSkipList = new List<string>();
            controlFileObject.LoanSuccessList = new List<string>();
            var attemptsToBeMade = fundingRequest.Where(x => x.PaymentType != PaymentType.Additional && x.PaymentType != PaymentType.Funding && x.PaymentType != PaymentType.Refund).ToList();
            var additionalSchedules = fundingRequest.Where(x => x.PaymentType == PaymentType.Additional && x.InstructionCreationDate.Time.Date <= tenantTime.Now.Date).ToList();

            controlFileObject.NoOfSchedulePaymentProcess = attemptsToBeMade.Where(x => x.RequestStatus == "initiated").ToList().Count;
            controlFileObject.NoOfResubmittedPaymentProcess = attemptsToBeMade.Where(x => x.RequestStatus == "resubmitted").ToList().Count;
            controlFileObject.NoOfAdditionalPaymentProcess = additionalSchedules.Count;

            attemptsToBeMade.AddRange(additionalSchedules);

            var successCount = 0;
            var skipCount = 0;
            var failCount = 0;
            double totalAmount = 0;
            int sequence = 1;
            List<RepaymentDetail> instructionDetails = new List<RepaymentDetail>();
            foreach (var item in attemptsToBeMade)
            {
                try
                {
                    controlFileObject.LoanProcessList.Add(item.LoanNumber);
                    var paymentDetails = await CreateInstruction(paymentService, tenantTime, ach, eventHub, loanService, ibvService, dataAttributeService, calendar, Configuration, item, instructionDetails, sequence);
                    sequence++;
                    if (paymentDetails.Item1 != null)
                    {
                        successCount++;
                        controlFileObject.LoanSuccessList.Add(item.LoanNumber);
                        totalAmount = totalAmount + paymentDetails.Item1.PaymentAmount;
                    }
                    else
                    {
                        controlFileObject.LoanSkipList.Add(item.LoanNumber);
                        skipCount++;
                    }
                    if (paymentDetails.Item1 != null)
                    {
                        instructionDetails.Add(paymentDetails.Item2);
                    }
                }
                catch (Exception ex)
                {
                    failCount++;
                    var exception = new ExceptionDetail();
                    exception.LoanNumber = item.EntityId;
                    exception.Exception = ex.Message;
                    controlFileObject.ExceptionDetails.Add(exception);
                    logger.Error($"Error while generating ACH Instructions for Loan {item.EntityId}", ex);
                }

            }

            if (Configuration.CreateInstructionCSV)
            {
                var funderWiseInstruction = instructionDetails.Select(x => x.FunderId).Distinct().ToList();
                foreach (var funder in funderWiseInstruction)
                {
                    var csvContent = CreateInstructionFileData(instructionDetails.Where(x => x.FunderId == funder).ToList());
                    if (csvContent != null)
                    {
                        var csvByteArray = Encoding.ASCII.GetBytes(csvContent.ToString());
                        var csvMemoryStream = new MemoryStream(csvByteArray);
                        UploadFile(csvMemoryStream, funder, Configuration, logger, tenantTime);
                    }
                }

            }
            controlFileObject.NoOfLoanSkip = skipCount;
            controlFileObject.NoOfLoanFail = failCount;
            controlFileObject.NoOfLoanSuccess = successCount;
            controlFileObject.TotalACHPulledAmount = totalAmount;
            return controlFileObject;
        }

        private static async Task<IFunding> FailAttempt(IPaymentProcessorService paymentService, ITenantTime tenantTime, IEventHubClient eventHub, ICalendarService calendar, Configuration Configuration, IFunding item)
        {
            var fundingData = await paymentService.UpdateFundingStatus(item.EntityType, item.EntityId, new UpdateFundingStatus { ReferenceId = item.ReferenceId, status = "failed" });
            var attempts = await paymentService.GetAttemptData(item.ReferenceId);
            await paymentService.AddFundingRequestAttempts(item.EntityType, item.EntityId, new PaymentRequestAttempts
            {
                ReferenceId = item.ReferenceId,
                AttemptDate = tenantTime.Now,
                FundingRequestAttemptId = Guid.NewGuid().ToString("N"),
                LoanNumber = item.LoanNumber,
                ReturnCode = Configuration.NSFCode,
                ReturnReason = Configuration.NSFReturnReason,
                ReturnDate = tenantTime.Now,
                BankAccountNumber = item.BankAccountNumber,
                BankAccountType = item.BankAccountType,
                BankRTN = item.BankRTN,
                BorrowersName = item.BorrowersName,
                AttemptCount = attempts != null && attempts.Count > 0 ? attempts.OrderByDescending(x => x.AttemptDate).FirstOrDefault().AttemptCount + 1 : 1
            });
            var nextRetryDate = GetNextRetryDay(tenantTime.Now.Date, Configuration.NextRetryDays, calendar);
            await paymentService.UpdateNextRetryDate("loan", item.LoanNumber, new UpdateRetryCount { ReferenceId = item.ReferenceId, nextRetryDate = nextRetryDate });

            await eventHub.Publish("FailedPayment", new
            {
                EntityId = item.LoanNumber,
                EntityType = "loan",
                Response = fundingData,
                LoanNumber = item.LoanNumber
            });

            await eventHub.Publish("NSFFailure", new
            {
                EntityId = item.LoanNumber,
                EntityType = "loan",
                Response = fundingData,
                LoanNumber = item.LoanNumber
            });
            return fundingData;
        }

        private static DateTimeOffset GetNextRetryDay(DateTimeOffset scheduleDate, int nextRetryDays, ICalendarService calendarService)
        {
            DateTimeOffset processingDate = scheduleDate;
            for (int i = 0; i < nextRetryDays; i++)
            {
                var todayInfo = calendarService.GetDate(processingDate.Year, processingDate.Month, processingDate.Day);
                processingDate = todayInfo.NextBusinessDay.Date;
            }
            return processingDate;
        }

        private async Task<Tuple<IFunding, RepaymentDetail>> AddInstruction(IPaymentProcessorService paymentService, ITenantTime tenantTime, IEventHubClient eventHub, IAchService ach, Configuration configuration, IFunding item, ILoanInformation loanInformation, ILoanManagementClientService loanService, ProductDetails productDetails, int sequence)
        {
            InstructionRequest instructionRequest = new InstructionRequest();
            instructionRequest.Amount = Convert.ToDecimal(item.PaymentAmount);
            instructionRequest.EntryDescription = "";
            instructionRequest.Frequency = 0;
            instructionRequest.FundingSourceId = configuration.FundingSourceId;
            Receiving receiving = new Receiving();
            receiving.AccountNumber = item.BankAccountNumber;
            receiving.AccountType = LendFoundry.MoneyMovement.Ach.AccountType.Individual;
            receiving.Name = item.BorrowersName;
            receiving.RoutingNumber = item.BankRTN;
            instructionRequest.Receiving = receiving;
            instructionRequest.ReferenceNumber = item.ReferenceId;
            instructionRequest.Type = item.BankAccountType.ToLower() == BankAccountType.Savings.ToString().ToLower() ? LendFoundry.MoneyMovement.Ach.TransactionType.DebitFromSavings : LendFoundry.MoneyMovement.Ach.TransactionType.DebitFromChecking;
            var instruction = await Task.Run(() => ach.Queue(instructionRequest));
            item.RequestStatus = "submitted";
            if (configuration.CreateInstructionCSV)
            {
                item.IsFileCreated = configuration.CreateInstructionCSV;
                item.FileCreatedDate = new TimeBucket(tenantTime.Now);
            }

            await Task.Run(() => paymentService.UpdateFundingData(item));
            var attempts = await paymentService.GetAttemptData(item.ReferenceId);
            var count = attempts != null && attempts.Count > 0 ? attempts.OrderByDescending(x => x.AttemptDate.Time).FirstOrDefault().AttemptCount + 1 : 1;
            await paymentService.AddFundingRequestAttempts(item.EntityType, item.EntityId, new PaymentRequestAttempts
            {
                ReferenceId = item.ReferenceId,
                AttemptDate = tenantTime.Now,
                FundingRequestAttemptId = Guid.NewGuid().ToString("N"),
                LoanNumber = item.LoanNumber,
                ProviderReferenceNumber = instruction.InternalReferenceNumber.ToString(),
                ReturnCode = string.Empty,
                BankAccountNumber = item.BankAccountNumber,
                BankAccountType = item.BankAccountType,
                BankRTN = item.BankRTN,
                BorrowersName = item.BorrowersName,
                ProviderSubmissionDate = tenantTime.Now,
                AttemptCount = count,
                IsFileCreated = configuration.CreateInstructionCSV
            });
            await eventHub.Publish(new InstructionCreated
            {
                EntityId = item.LoanNumber,
                EntityType = "loan",
                Response = item
            });

            RepaymentDetail instructionDetails = null;
            if (configuration.CreateInstructionCSV)
            {
                var activeSchedule = await loanService.GetLoanSchedule(loanInformation.LoanNumber);
                var currentPayment = activeSchedule != null && activeSchedule.ScheduleDetails != null && activeSchedule.ScheduleDetails.Any()
                                        ? activeSchedule.ScheduleDetails.Where(a => a.ScheduleDate.Date == item.PaymentScheduleDate.Time.Date).FirstOrDefault()
                                        : null;

                instructionDetails = new RepaymentDetail
                {
                    SrNo = sequence,
                    Date = tenantTime.Now.Date,
                    ProductName = productDetails != null && productDetails.Product != null ? productDetails.Product.Name : string.Empty,
                    BorrowerName = loanInformation.PrimaryApplicantDetails != null && loanInformation.PrimaryApplicantDetails.Name != null ? $"{loanInformation.PrimaryApplicantDetails.Name.First} {loanInformation.PrimaryApplicantDetails.Name.Last}" : string.Empty,
                    LoanNumber = loanInformation.LoanNumber,
                    FunderId = loanInformation.FunderId,
                    ApplicationNumber = loanInformation.LoanApplicationNumber,
                    AdditionalReferenceId = loanInformation.AdditionalRefId,
                    ScheduleDate = item.PaymentScheduleDate.Time.Date,
                    ReceivedAmount = item.PaymentAmount,
                    ReferenceNumber = instruction.ReferenceNumber,
                    InternalReferenceNumber = instruction.InternalReferenceNumber,
                    ActualEmi = item.PaymentAmount,
                    InstallmentNumber = currentPayment != null ? currentPayment.Installmentnumber : 0
                };
                return Tuple.Create(item, instructionDetails);

            }
            return Tuple.Create(item, instructionDetails);
        }

        private async Task<ProductDetails> GetProductParameters(string loanNumber, IDataAttributesEngine dataAttributesEngine)
        {
            var productDetails = await dataAttributesEngine.GetAttribute("loan", loanNumber, "product");
            var productObject = JsonConvert.DeserializeObject<List<ProductDetails>>(productDetails.ToString());
            return productObject.FirstOrDefault();
        }

        private StringBuilder CreateInstructionFileData(List<RepaymentDetail> instructions)
        {
            if (instructions == null || !instructions.Any())
            {
                return null;
            }
            var properties = typeof(RepaymentDetail).GetProperties().Select(x => x.Name).ToList();
            var csv = new StringBuilder();
            var csvHeader = string.Join(",", properties);
            csv.AppendLine(csvHeader);
            foreach (var item in instructions)
            {
                var row = string.Empty;
                foreach (var property in properties)
                {
                    if (!string.IsNullOrWhiteSpace(row))
                    {
                        row += ",";
                    }
                    row += GetPropValue(item, property);
                }
                csv.AppendLine(row);
            }
            return csv;
        }

        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        private SftpClient GetSftpClient(Configuration configuration)
        {
            return new SftpClient(configuration.Provider.Host, configuration.Provider.Port, configuration.Provider.Username, configuration.Provider.Password);
        }

        private void UploadFile(Stream content, string funder, Configuration configuration, ILogger logger, ITenantTime tenantTime)
        {
            using (var client = GetSftpClient(configuration))
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);
                try
                {
                    FaultRetry.RunWithAlwaysRetry(
                        () =>
                        {
                            client.UploadFile(content, configuration.Provider.OutboxLocation + configuration.RepaymentFileName + "_" + funder + "_" + tenantTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv", true);
                        });
                }
                catch (Exception exception)
                {
                    logger.Error("Unable to upload file", exception);
                }
            }
        }

    }
}
