﻿using LMS.Payment.Processor;

namespace LMS.Tasks.Loan.CreateInstruction
{
    public class InstructionCreated
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public IFunding Response { get; set; }
     
    }
}
