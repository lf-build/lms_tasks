using LendFoundry.Foundation.Services.Settings;
using System;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public static class Settings
    {
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-process-payment-migration";
        
    }
}