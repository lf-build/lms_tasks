using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;
using LendFoundry.EventHub.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using Renci.SshNet;
using System.Text;
using System.IO;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting;
using LMS.Payment.Processor.Client;
using LMS.Payment.Processor;
using LMS.LoanManagement.Abstractions;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public class Agent : ScheduledAgent
    {

        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ILoanManagementClientServiceFactory loanManagementClientServiceFactory,
            IAccrualBalanceClientFactory accrualBalanceClientFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
            LoanManagementClientServiceFactory = loanManagementClientServiceFactory;
            AccrualBalanceClientFactory = accrualBalanceClientFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
        }

        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ILoanManagementClientServiceFactory LoanManagementClientServiceFactory { get; }
        private IAccrualBalanceClientFactory AccrualBalanceClientFactory { get; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventHub = EventHubFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            var loanManagementClientService = LoanManagementClientServiceFactory.Create(reader);
            var accrualBalanceService = AccrualBalanceClientFactory.Create(reader);
            var paymentProcessorService = PaymentServiceClientFactory.Create(reader);
            if (configurationService == null)
            {
                logger.Error($"Configuration service is null");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error($"Configuration was not found of {Settings.ServiceName}");
                return;
            }
            try
            {
                logger.Info("Started processing payment migration");

                if (Configuration.Loans != null && Configuration.Loans.Any())
                {
                    foreach (var loan in Configuration.Loans)
                    {
                        try
                        {
                            logger.Info($"Processing started for loan: {loan}");
                            await ProcessPaymentMigration(Configuration, loan, loanManagementClientService, accrualBalanceService, paymentProcessorService, logger);
                            logger.Info($"Processing finished for loan: {loan}");
                        }
                        catch (Exception ex)
                        {
                            logger.Info($"Error while processing for loan: {loan} with error message as {ex.Message}");
                        }
                    }
                }
                else
                {
                    if (Configuration.Provider == null)
                    {
                        throw new ArgumentException($"Provider details not provided");
                    }
                    if (string.IsNullOrWhiteSpace(Configuration.Provider.Host) || Configuration.Provider.Port <= 0 || string.IsNullOrWhiteSpace(Configuration.Provider.Username) || string.IsNullOrWhiteSpace(Configuration.Provider.Password))
                    {
                        throw new ArgumentException($"Missing some required provider details");
                    }
                    var files = DownloadFiles(Configuration.Provider.InboxLocation, Configuration.Provider.DestinationFileLocation, Configuration, logger);
                    // Fetch from CSV file
                    await ReadFile(Configuration.Provider.InboxLocation, Configuration.Provider.DestinationFileLocation, Configuration, eventHub, logger, tenantTime, loanManagementClientService, accrualBalanceService, paymentProcessorService);

                    foreach (var file in files)
                    {
                        File.Delete(Path.Combine(Configuration.Provider.DestinationFileLocation, file));
                    }
                }
                logger.Info("Ended processing payment migration");
            }
            catch (Exception e)
            {
                logger.Error("Error while processing payment migration", e);
            }
        }

        private SftpClient GetSftpClient(Configuration configuration)
        {
            return new SftpClient(configuration.Provider.Host, configuration.Provider.Port, configuration.Provider.Username, configuration.Provider.Password);
        }

        public List<string> DownloadFiles(string sourcePath, string destinationPath, Configuration configuration, ILogger logger)
        {
            if (string.IsNullOrWhiteSpace(sourcePath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(sourcePath));
            }
            if (string.IsNullOrWhiteSpace(destinationPath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));
            }

            var fileNameList = new List<string>();
            var inboxLocation = sourcePath;

            using (var client = GetSftpClient(configuration))
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                foreach (var file in client.ListDirectory(inboxLocation))
                {
                    if (file.IsRegularFile)
                    {
                        try
                        {
                            FaultRetry.RunWithAlwaysRetry(
                                () =>
                                {
                                    if (!File.Exists(destinationPath))
                                    {
                                        Directory.CreateDirectory(destinationPath);
                                    }
                                    using (var fs = new FileStream(Path.Combine(destinationPath, file.Name), FileMode.Create))
                                    {
                                        client.DownloadFile(file.FullName, fs);
                                        fs.Close();
                                    }
                                });

                            fileNameList.Add(file.Name);
                        }
                        catch (Exception exception)
                        {
                            logger.Error("Unable to download file", exception);
                        }
                    }
                }
            }
            return fileNameList;
        }

        private async Task ReadFile(string sourcePath, string destinationPath, Configuration configuration, IEventHubClient eventHub, ILogger logger, ITenantTime tenantTime, ILoanManagementClientService loanManagementClientService, IAccrualBalanceService accrualBalanceService, IPaymentProcessorService paymentProcessorService)
        {
            if (string.IsNullOrWhiteSpace(sourcePath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(sourcePath));
            }

            if (string.IsNullOrWhiteSpace(destinationPath))
            {
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));
            }

            var inboxLocation = sourcePath;

            using (var client = GetSftpClient(configuration))
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);
                Dictionary<PaymentMigrationDetail, string> failedProcessing = new Dictionary<PaymentMigrationDetail, string>();
                foreach (var file in client.ListDirectory(inboxLocation))
                {
                    if (file.IsRegularFile)
                    {
                        bool isFailed = false;
                        bool isError = false;
                        try
                        {
                            var fileContent = ValidateFile(file.Name, destinationPath, configuration, logger);
                            List<PaymentMigrationDetail> paymentMigrationDetails = fileContent
                                    .Skip(1)
                                    .Select(v => PaymentMigrationDetail.FromCsv(v, configuration.CSVColumnFormat))
                                    .ToList();

                            foreach (var paymentMigrationDetail in paymentMigrationDetails)
                            {
                                try
                                {
                                    logger.Info($"Processing started for loan: {paymentMigrationDetail.LoanNumber}");
                                    await ProcessPaymentMigration(configuration, paymentMigrationDetail.LoanNumber, loanManagementClientService, accrualBalanceService, paymentProcessorService, logger);
                                    logger.Info($"Processing finished for loan: {paymentMigrationDetail.LoanNumber}");
                                }
                                catch (Exception ex)
                                {
                                    logger.Info($"Error while processing for loan: {paymentMigrationDetail.LoanNumber} with error message as {ex.Message}");
                                    failedProcessing.Add(paymentMigrationDetail, ex.Message);
                                }
                            }

                            var csvContent = CreateFailedProcessingFile(failedProcessing, configuration.CSVColumnFormat);
                            if (csvContent != null)
                            {
                                var csvByteArray = Encoding.ASCII.GetBytes(csvContent.ToString());
                                var csvMemoryStream = new MemoryStream(csvByteArray);
                                isFailed = true;
                                client.UploadFile(csvMemoryStream, Path.Combine(configuration.Provider.FailedLocation, configuration.FailedFileName + "_" + tenantTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv"), true);
                            }
                        }
                        catch (Exception exception)
                        {
                            isError = true;
                            logger.Error($"Unable to process file {file.Name}", exception);
                        }
                        finally
                        {
                            var fileContent = File.ReadAllBytes(Path.Combine(destinationPath, file.Name));
                            var csvMemoryStream = new MemoryStream(fileContent);
                            client.UploadFile(csvMemoryStream, Path.Combine(configuration.Provider.ArchiveLocation, file.Name));
                            if (!isFailed && isError)
                            {
                                FileInfo fileInfo = new FileInfo(file.Name);
                                csvMemoryStream = new MemoryStream(fileContent);
                                var fileName = configuration.FailedFileName + "_(Invalid_Extension_Header_Content)" + "_" + tenantTime.Now.ToString("yyyyMMddHHmmssfff") + fileInfo.Extension;
                                client.UploadFile(csvMemoryStream, Path.Combine(configuration.Provider.FailedLocation, fileName));
                            }
                            client.DeleteFile(Path.Combine(sourcePath, file.Name));
                        }
                    }
                }
            }
        }

        private string[] ValidateFile(string fileName, string sourcePath, Configuration configuration, ILogger logger)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Extension != ".csv")
            {
                logger.Error($"Invalid file {fileName}");
                throw new Exception($"Invalid file {fileName}");
            }
            var fileContent = File.ReadAllLines(Path.Combine(sourcePath, fileName));
            if (fileContent == null || fileContent.Count() <= 1)
            {
                logger.Error($"No content in {fileName}");
                throw new Exception($"No content in {fileName}");
            }
            var headers = fileContent.FirstOrDefault();
            var expectedHeaders = string.Join(",", configuration.CSVColumnFormat);
            if (!string.Equals(headers, expectedHeaders, StringComparison.InvariantCultureIgnoreCase))
            {
                logger.Error($"Incorrect header format for {fileName}");
                throw new Exception($"Incorrect header format for {fileName}");
            }
            return fileContent;
        }

        private StringBuilder CreateFailedProcessingFile(Dictionary<PaymentMigrationDetail, string> failedProcessing, List<string> headers)
        {
            if (failedProcessing == null || failedProcessing.Count() <= 0)
            {
                return null;
            }
            var csv = new StringBuilder();
            headers.Add("Exception");
            var csvHeader = string.Join(",", headers);
            csv.AppendLine(csvHeader);
            foreach (var item in failedProcessing)
            {
                var row = string.Empty;
                foreach (var header in headers)
                {
                    if (!string.IsNullOrWhiteSpace(row))
                    {
                        row += ",";
                    }
                    row += GetPropValue(item, header);
                }
                row += $",{item.Value}";
                csv.AppendLine(row);
            }
            return csv;
        }

        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        private async Task ProcessPaymentMigration(Configuration configuration, string loanNumber, ILoanManagementClientService loanManagementClientService, IAccrualBalanceService accrualBalanceService, IPaymentProcessorService paymentProcessorService, ILogger logger)
        {
            // STEP 1 -> Get loan details and schedule of loan
            var loanInformation = await loanManagementClientService.GetLoanInformationByLoanNumber(loanNumber);
            // STEP 2 -> Get all schedules less than PaymentProcessEndDate
            var loanSchedule = await loanManagementClientService.GetLoanSchedule(loanNumber);
            if (loanSchedule == null || loanSchedule.ScheduleDetails == null || !loanSchedule.ScheduleDetails.Any())
            {
                return;
            }
            var accrualBalances = await accrualBalanceService.GetLoanByScheduleVersion(loanNumber, loanSchedule.ScheduleVersionNo);
            if (accrualBalances == null)
            {
                return;
            }
            var scheduleTracking = await accrualBalanceService.GetScheduleByLoanNumberVersion(loanNumber, loanSchedule.ScheduleVersionNo);
            // STEP 3 -> Find accrual dates for respective schedule
            var processingSchedules = loanSchedule.ScheduleDetails.Where(x => x.ScheduleDate.Date <= configuration.PaymentProcessEndDate.Date).ToList();
            foreach (var schedule in processingSchedules)
            {
                if (scheduleTracking != null && scheduleTracking.ScheduleTracking != null && scheduleTracking.ScheduleTracking.Any())
                {
                    var scheduleTrackingByInstallmentNumber = scheduleTracking.ScheduleTracking.Where(x => x.Installmentnumber == schedule.Installmentnumber).ToList();
                    if (scheduleTrackingByInstallmentNumber.Any(x => x.IsPaid))
                    {
                        continue;
                    }
                }
                // STEP 4 -> Run accrual for those dates
                DateTimeOffset startDate;
                DateTimeOffset endDate;
                if (schedule.Installmentnumber == 1)
                {
                    startDate = loanSchedule.FundedDate.Time;
                }
                else
                {
                    var previousSchedule = loanSchedule.ScheduleDetails.Where(x => x.Installmentnumber < schedule.Installmentnumber).OrderByDescending(x => x.ScheduleDate).FirstOrDefault();
                    startDate = previousSchedule.ScheduleDate;
                }
                endDate = schedule.ScheduleDate.AddDays(-1);
                if (accrualBalances.PBOT != null && accrualBalances.PBOT.AsOfDatePBOT.Date > startDate.Date)
                {
                    startDate = accrualBalances.PBOT.AsOfDatePBOT;
                }
                if (accrualBalances.PBOT != null && accrualBalances.PBOT.AsOfDatePBOT.Date == endDate.Date) { }
                else
                {
                    var accrualRequest = new AccrualRequest() { StartDate = startDate, EndDate = endDate };
                    logger.Info($"Accrual started for loan: {loanNumber}", accrualRequest);
                    await accrualBalanceService.Accrual(loanNumber, accrualRequest);
                    logger.Info($"Accrual finished for loan: {loanNumber}", accrualRequest);
                }
                var paymentRequest = new FundingRequest()
                {
                    PaymentType = Payment.Processor.PaymentType.Manual,
                    PaymentInstrumentType = PaymentInstrumentType.Cash,
                    PaymentPurpose = "schedule",
                    PaymentAmount = schedule.PaymentAmount,
                    TransactionType = Payment.Processor.TransactionType.Credit,
                    EffectiveDate = schedule.OriginalScheduleDate,
                    RequestedDate = schedule.ScheduleDate,
                    PaymentScheduleDate = schedule.ScheduleDate
                };
                logger.Info($"Payment started for loan: {loanNumber}", paymentRequest);
                // STEP 5 -> Make payment for fetched schedule
                var payment = await paymentProcessorService.AddManualPayment("loan", loanNumber, paymentRequest, null);
                logger.Info($"Payment finished for loan: {loanNumber}", paymentRequest);
            }
            accrualBalances = await accrualBalanceService.GetLoanByScheduleVersion(loanNumber, loanSchedule.ScheduleVersionNo);
            // STEP 6 -> Accrual in case after payments some days are past
            if (configuration.PaymentProcessEndDate.Date > accrualBalances.PBOT.AsOfDatePBOT.Date && accrualBalances.PayOff == null)
            {
                var accrualRequest = new AccrualRequest() { StartDate = accrualBalances.PBOT.AsOfDatePBOT.AddDays(1), EndDate = configuration.PaymentProcessEndDate };
                logger.Info($"Extra accrual started for loan: {loanNumber}", accrualRequest);
                await accrualBalanceService.Accrual(loanNumber, accrualRequest);
                logger.Info($"Extra accrual finished for loan: {loanNumber}", accrualRequest);
            }
        }
    }
}
