﻿
using LendFoundry.Foundation.Client;
using System;
using System.Collections.Generic;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public class Configuration : IDependencyConfiguration
    {
        public List<string> Loans { get; set; }
        public List<string> CSVColumnFormat { get; set; }
        public Provider Provider { get; set; }
        public string FailedFileName { get; set; }
        public DateTimeOffset PaymentProcessEndDate { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}
