namespace LMS.Tasks.Loan.PaymentMigration
{
    public interface IProvider
    {
        string Host { get; set; }
        int Port { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        ProviderProtocol Protocol { get; set; }
        string InboxLocation { get; set; }
        string ArchiveLocation { get; set; }
        string FailedLocation { get; set; }
    }
}