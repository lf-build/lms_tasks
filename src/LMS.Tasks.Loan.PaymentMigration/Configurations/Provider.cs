namespace LMS.Tasks.Loan.PaymentMigration
{
    public class Provider : IProvider
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public ProviderProtocol Protocol { get; set; }
        public string InboxLocation { get; set; }
        public string ArchiveLocation { get; set; }
        public string FailedLocation { get; set; }
        public string DestinationFileLocation { get; set; }
    }
}