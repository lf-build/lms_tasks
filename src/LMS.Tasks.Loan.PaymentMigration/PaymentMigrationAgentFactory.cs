﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LMS.Payment.Processor.Client;
using LendFoundry.Foundation.Logging;
using LMS.LoanAccounting;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;
using LMS.LoanManagement.Client;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public class PaymentMigrationAgentFactory : IPaymentMigrationAgentFactory
    {

        public PaymentMigrationAgentFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var accrualBalanceClientFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loanManagementClientServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory, eventHubClientFactory, loggerFactory, tenantServiceFactory, loanManagementClientServiceFactory, accrualBalanceClientFactory, paymentServiceClientFactory);
        }
    }
}
