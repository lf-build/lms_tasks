﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.Payment.Processor.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tasks;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting.Client;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public class Program : LendFoundry.Tasks.DependencyInjection
    {
        public static void Main(string[] args)
        {
            //Provider.GetService<IAgent>().Execute();
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();

            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddPaymentProcessorService();
            services.AddLoanManagementService();
            services.AddAccrualBalance();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<ITaskRepositoryFactory, TaskRepositoryFactory>();
            services.AddTransient<IControlFileFactory, MongoProviderFactory>();
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName); 
            services.AddTransient<IAgent, Agent>();
            return services;
        }
    }
}
