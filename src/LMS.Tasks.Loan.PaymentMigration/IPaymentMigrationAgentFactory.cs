﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public interface IPaymentMigrationAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}