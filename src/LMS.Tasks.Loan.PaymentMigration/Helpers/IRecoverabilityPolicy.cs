using System;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}