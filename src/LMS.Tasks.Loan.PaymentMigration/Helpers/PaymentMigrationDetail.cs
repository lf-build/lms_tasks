using System;
using System.Collections.Generic;

namespace LMS.Tasks.Loan.PaymentMigration
{
    public class PaymentMigrationDetail
    {
        public string LoanNumber { get; set; }


        public static PaymentMigrationDetail FromCsv(string csvLine, List<string> csvFormat)
        {
            string[] values = csvLine.Split(',');
            PaymentMigrationDetail paymentMigrationDetail = new PaymentMigrationDetail();
            if(csvFormat.IndexOf("LoanNumber") >= 0)
            {
                paymentMigrationDetail.LoanNumber = Convert.ToString(values[csvFormat.IndexOf("LoanNumber")]);
            }            
            return paymentMigrationDetail;
        }
    }
}