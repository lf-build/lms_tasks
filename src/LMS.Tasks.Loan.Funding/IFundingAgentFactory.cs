﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.Funding
{
    public interface IFundingAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}