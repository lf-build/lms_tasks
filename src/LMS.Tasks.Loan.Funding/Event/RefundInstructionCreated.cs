﻿
using LMS.Payment.Processor;

namespace LMS.Tasks.Loan.Funding
{
    public class RefundInstructionCreated
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public IFunding Response { get; set; }
    }
}
