using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LMS.LoanAccounting;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.Funding
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory,
            IEventHubClientFactory eventHubFactory,
            IAchServiceFactory achServiceFactory,
             IAccrualBalanceClientFactory accrualServiceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IControlFileFactory controlFileFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            AccrualServiceFactory = accrualServiceFactory;
            LoggerFactory = loggerFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
            EventHubFactory = eventHubFactory;
            AchServiceFactory = achServiceFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;

        }
        private IControlFileFactory ControlFileFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IAchServiceFactory AchServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IAccrualBalanceClientFactory AccrualServiceFactory { get; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }
        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var paymentService = PaymentServiceClientFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var eventHub = EventHubFactory.Create(reader);
            var ach = AchServiceFactory.Create(reader);
            var accrual = AccrualServiceFactory.Create(reader);
            var contolFile = ControlFileFactory.Create(reader);

            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the configuration related to refund task");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Was not found the configuration related to refund task");
                return;
            }
            try
            {
                var controlFileObject = new ControlDetail();
                controlFileObject.TaskName = Settings.ServiceName;
                controlFileObject.StartTime = tenantTime.Today;
                controlFileObject.ExceptionDetails = new List<ExceptionDetail>();
                controlFileObject.LoanProcessList = new List<string>();
                controlFileObject.LoanSkipList = new List<string>();
                controlFileObject.LoanSuccessList = new List<string>();
                logger.Info("Started Creating Payment Pull Request");

                logger.Info("Started refund task");
                var fundingRequest = await paymentService.GetFundingRequest(Configuration.FundingStatus);
                var attemptsToBeMade = fundingRequest.Where(x => (x.PaymentType == Payment.Processor.PaymentType.Refund || x.PaymentType == Payment.Processor.PaymentType.Funding) && x.PaymentScheduleDate.Time.Date <= tenantTime.Now.Date).ToList();
                controlFileObject.NoOfLoanProcess = attemptsToBeMade.Count;
                var successCount = 0;
                var skipCount = 0;
                var failCount = 0;
                foreach (var item in attemptsToBeMade)
                {
                    try
                    {
                        controlFileObject.LoanProcessList.Add(item.LoanNumber);
                        var paymentDetails = await MakeFundingRequest(paymentService, tenantTime, eventHub, ach, accrual, Configuration, item);
                        if (paymentDetails != null)
                        {
                            controlFileObject.LoanSuccessList.Add(item.LoanNumber);
                            successCount++;
                        }
                        else
                        {
                            controlFileObject.LoanSkipList.Add(item.LoanNumber);
                            skipCount++;
                        }

                    }
                    catch (Exception ex)
                    {
                        failCount++;
                        var exception = new ExceptionDetail();
                        exception.LoanNumber = item.LoanNumber;
                        exception.Exception = ex.Message;
                        controlFileObject.ExceptionDetails.Add(exception);
                        logger.Error($"Error while refund payment for loan {item.LoanNumber}", ex);
                    }
                }
                controlFileObject.NoOfLoanSkip = skipCount;
                controlFileObject.NoOfLoanFail = failCount;
                controlFileObject.NoOfLoanSuccess = successCount;
                contolFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject,tenant);
                logger.Info("Ended refund task");
            }
            catch (Exception e)
            {
                logger.Error("Error while refund task", e);
            }
        }

        public async Task<IInstruction> MakeFundingRequest(IPaymentProcessorService paymentService, ITenantTime tenantTime, IEventHubClient eventHub, IAchService ach, IAccrualBalanceService accrual, Configuration Configuration, IFunding item)
        {
            if (item.PaymentRail == PaymentRail.ACH)
            {
                InstructionRequest instructionRequest = new InstructionRequest();
                instructionRequest.Amount = Convert.ToDecimal(item.PaymentAmount);
                instructionRequest.EntryDescription = "";
                instructionRequest.Frequency = 0;
                instructionRequest.FundingSourceId = string.IsNullOrWhiteSpace(item.ACHFundingSourceId) ? Configuration.FundingSourceId : item.ACHFundingSourceId;
                Receiving receiving = new Receiving();
                receiving.AccountNumber = item.BankAccountNumber;
                receiving.AccountType = LendFoundry.MoneyMovement.Ach.AccountType.Individual;
                receiving.Name = item.BorrowersName;
                receiving.RoutingNumber = item.BankRTN;
                instructionRequest.Receiving = receiving;
                instructionRequest.ReferenceNumber = item.ReferenceId;
                instructionRequest.StandardEntryClassCode = "WEB";
                instructionRequest.Type = item.BankAccountType.ToLower() == BankAccountType.Savings.ToString().ToLower() ? LendFoundry.MoneyMovement.Ach.TransactionType.CreditToSavings : LendFoundry.MoneyMovement.Ach.TransactionType.CreditToChecking;
                var instruction = await Task.Run(() => ach.Queue(instructionRequest));

                var funding = await paymentService.UpdateFundingStatus(item.EntityType, item.EntityId, new UpdateFundingStatus { ReferenceId = item.ReferenceId, status = "submitted" });

                await paymentService.AddFundingRequestAttempts(item.EntityType, item.EntityId, new PaymentRequestAttempts
                {
                    ReferenceId = item.ReferenceId,
                    AttemptDate = tenantTime.Now,
                    FundingRequestAttemptId = Guid.NewGuid().ToString("N"),
                    FundingRequestID = item.Id,
                    LoanNumber = item.LoanNumber,
                    ProviderReferenceNumber = instruction.InternalReferenceNumber.ToString(),
                    ReturnCode = string.Empty,
                    BankAccountNumber = item.BankAccountNumber,
                    BankAccountType = item.BankAccountType,
                    BankRTN = item.BankRTN,
                    BorrowersName = item.BorrowersName,
                    ProviderSubmissionDate = tenantTime.Now
                });

                await eventHub.Publish(new RefundInstructionCreated
                {
                    EntityId = item.LoanNumber,
                    EntityType = "loan",
                    Response = funding
                });
                return instruction;
            }
            return null;
        }
    }
}
