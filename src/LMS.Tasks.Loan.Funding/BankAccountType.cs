﻿
namespace LMS.Tasks.Loan.Funding
{
    public enum BankAccountType
    {
        Savings,
        Current,
        Checking
    }
}
