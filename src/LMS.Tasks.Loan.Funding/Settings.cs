using LendFoundry.Foundation.Services.Settings;
using System;

namespace LMS.Tasks.Loan.Funding
{
    public static class Settings
    {
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-funding-payment";
       
    }
}