﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
namespace LMS.Tasks.Loan.Funding
{
    public class Configuration :IDependencyConfiguration 
    {
        public List<string> FundingStatus { get; set; }

        public string FundingSourceId { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
