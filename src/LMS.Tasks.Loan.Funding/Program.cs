﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.LoanAccounting.Client;
using LMS.Payment.Processor.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tasks;
using LendFoundry.Foundation.ServiceDependencyResolver;
namespace LMS.Tasks.Loan.Funding
{
    public class Program : LendFoundry.Tasks.DependencyInjection
    {
        public static void Main(string[] args)
        {
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
        }
        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddPaymentProcessorService();
            services.AddAchService();
            services.AddAccrualBalance();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTenantService();
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName); 
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<ITaskRepositoryFactory, TaskRepositoryFactory>();
            services.AddTransient<IControlFileFactory, MongoProviderFactory>();

            return services;
        }
    }
}
