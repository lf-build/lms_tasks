﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.Framework.DependencyInjection;

#endif
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.EventHub.Client;
using LMS.Payment.Processor.Client;
using LMS.LoanAccounting;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;

namespace LMS.Tasks.Loan.Funding
{
    public class FundingAgentFactory : IFundingAgentFactory
    {


        public FundingAgentFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public Agent Create(ITokenReader reader)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tokenFactory = Provider.GetService<ITokenHandler>();
            var tenantFactory = Provider.GetService<ITenantTimeFactory>();
            var eventHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var paymentServiceClientFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var accrual = Provider.GetService<IAccrualBalanceClientFactory>();
            var achServiceFactory = Provider.GetService<IAchServiceFactory>();
            var controlFileFactory = Provider.GetService<IControlFileFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            return new Agent(configurationServiceFactory, tokenFactory, tenantFactory, paymentServiceClientFactory, eventHubClientFactory, achServiceFactory, accrual, loggerFactory, tenantServiceFactory, controlFileFactory);
        }
    }
}
