using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Client;
using LMS.LoanManagement.Client;
using LendFoundry.Calendar.Client;
using LMS.Payment.Processor.Client;
using LMS.Payment.Processor;
using System.Collections.Generic;
using System.Threading.Tasks;
using LMS.LoanAccounting;
using LendFoundry.EventHub;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using LMS.LoanManagement.Abstractions;

namespace LMS.Tasks.Loan.CreateFeePullRequest
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ILoanFilterClientServiceFactory loanFilterClientServiceFactory,
            ILoanManagementClientServiceFactory loanManagementClientService,
            IEventHubClientFactory eventHubFactory,
            ICalendarServiceFactory calendarServiceFactory,
            IPaymentServiceClientFactory paymentServiceClientFactory,
            IAccrualBalanceClientFactory accrualServiceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
             IControlFileFactory controlFileFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            LoanManagementClientService = loanManagementClientService;
            LoanFilterClientServiceFactory = loanFilterClientServiceFactory;
            PaymentServiceClientFactory = paymentServiceClientFactory;
            CalendarServiceFactory = calendarServiceFactory;
            EventHubFactory = eventHubFactory;
            AccrualServiceFactory = accrualServiceFactory;
            ControlFileFactory = controlFileFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private IControlFileFactory ControlFileFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IAccrualBalanceClientFactory AccrualServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }

        private ILoanFilterClientServiceFactory LoanFilterClientServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ICalendarServiceFactory CalendarServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        //    private IStatusManagementServiceFactory StatusManagementFactory { get; }
        private ILoanManagementClientServiceFactory LoanManagementClientService { get; set; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }

        private static string EntityType = "loan";
        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName, null, "system", null);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var loanSchedule = LoanManagementClientService.Create(reader);
            var loanFilterService = LoanFilterClientServiceFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var calendar = CalendarServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var paymentService = PaymentServiceClientFactory.Create(reader);
            var accrual = AccrualServiceFactory.Create(reader);
            var contolFile = ControlFileFactory.Create(reader);

            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the Configuration related to payment pull request Task");
                return;
            }

            var Configuration = configurationService.Get();
            if (Configuration == null)
            {
                logger.Error("Was not found the Configuration related to payment pull request");
                return;
            }
            try
            {
                var controlFileObject = new ControlDetail();
                controlFileObject.TaskName = Settings.ServiceName;
                controlFileObject.StartTime = tenantTime.Today;
                controlFileObject.ExceptionDetails = new List<ExceptionDetail>();
                controlFileObject.LoanProcessList = new List<string>();
                controlFileObject.LoanSkipList = new List<string>();
                controlFileObject.LoanSuccessList = new List<string>();

                logger.Info($"Started Creating Payment Pull Request for tenant {tenant}");

                var loans = await loanFilterService.GetAllByStatus(Configuration.LoanStatus);
                var successCount = 0;
                var processCount = 0;
                var skipCount = 0;
                var failCount = 0;
                double totalAmount = 0;
                foreach (var loan in loans)
                {
                    try
                    {
                        var processingEndDate = tenantTime.Today.UtcDateTime;
                        // var processingEndDate = new DateTimeOffset(new DateTime(2018, 1, 07));

                        for (int i = 0; i < Configuration.NotificationSendDays; i++)
                        {
                            var todayInfo = calendar.GetDate(processingEndDate.Year, processingEndDate.Month, processingEndDate.Day);
                            processingEndDate = todayInfo.NextBusinessDay.Date;
                        }
                        var currentSchedule = await loanSchedule.GetLoanSchedule(loan.LoanNumber);
                        var loanDetails = await loanSchedule.GetLoanInformationByLoanNumber(loan.LoanNumber);
                        if(!loanDetails.IsAutoPay )
                        {
                            continue;
                        }
                        var scheduleToBeProcess = currentSchedule.FeeDetails.Where(x => x.FeeDueDate.Date <= processingEndDate.Date && x.FeeType == "Scheduled" && x.IsProcessed == false).ToList();
                        foreach (var schedule in scheduleToBeProcess)
                        {
                            processCount++;
                            logger.Info($"Now processing for loan : {loan.LoanNumber} for tenant {tenant}");

                            controlFileObject.LoanProcessList.Add(loan.LoanNumber);
                            var paymentDetails = await InitiateFeePullRequest(loanSchedule, paymentService, tenantTime, eventHub, Configuration, schedule, accrual, loanDetails);
                            if (paymentDetails != null)
                            {
                                successCount++;
                                controlFileObject.LoanSuccessList.Add(loan.LoanNumber);
                                totalAmount = totalAmount + paymentDetails.PaymentAmount;
                            }
                            else
                            {
                                controlFileObject.LoanSkipList.Add(loan.LoanNumber);
                                skipCount++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        failCount++;
                        var exception = new ExceptionDetail();
                        exception.LoanNumber = loan.LoanNumber;
                        exception.Exception = ex.Message;
                        controlFileObject.ExceptionDetails.Add(exception);
                        logger.Error($"Error while Processing Request for loan : {loan.LoanNumber}", ex);
                    }
                }
                controlFileObject.NoOfLoanProcess = processCount;
                controlFileObject.NoOfLoanSkip = skipCount;
                controlFileObject.NoOfLoanFail = failCount;
                controlFileObject.NoOfLoanSuccess = successCount;
                controlFileObject.TotalFeePulledAmount = totalAmount;
                contolFile.AddControlFile(controlFileObject.TaskName, controlFileObject.StartTime, controlFileObject,tenant);
                logger.Info("Ended Creating Payment Pull Request");

            }
            catch (Exception e)
            {
                logger.Error("Error while generating ACH Instructions", e);
            }

        }

        public async Task<IFunding> InitiateFeePullRequest(ILoanManagementClientService loanSchedule, IPaymentProcessorService paymentService, ITenantTime tenantTime, IEventHubClient eventHub, Configuration Configuration, LoanManagement.Abstractions.IFeeDetails schedule, IAccrualBalanceService accrual,   ILoanInformation loanDetails)
        {
            if (schedule != null)
            {
                var bankDetail = await loanSchedule.GetBankDetails(loanDetails.LoanNumber);
                var instrument = loanDetails.PaymentInstruments.FirstOrDefault(x => x.Id == bankDetail.PaymentInstrumentId);
                if (instrument == null)
                    throw new NotFoundException($"Payment InstrmentId {bankDetail.PaymentInstrumentId} is not found in loan details");

                if (schedule.IsProcessed == false && schedule.FeeAmount > 0 && loanDetails.IsAutoPay == true)
                {
                    IFundingRequest request = new FundingRequest();
                    request.BankAccountNumber = bankDetail.AccountNumber.ToString();
                    request.BankAccountType = bankDetail.AccountType.ToString();
                    request.BankRTN = bankDetail.RoutingNumber.ToString();
                    request.BorrowersName = bankDetail.BankHolderName;
                    request.PaymentScheduleDate = schedule.FeeDueDate;
                    request.RequestedDate = tenantTime.Now;
                    request.Fees = new List<Payment.Processor.IFeeDetails>();
                    var fee = new Payment.Processor.FeeDetails();
                    fee.Id = schedule.FeeId;
                    fee.FeeName = schedule.FeeName;
                    fee.FeeAmount = schedule.FeeAmount;
                    request.Fees.Add(fee);

                    request.PaymentPurpose = Configuration.CustomPurpose;
                    request.StatusUpdatedDate = tenantTime.Now;
                    request.Status = "initiated";
                    request.PaymentInstrumentType = instrument.PaymentInstrumentType;
                    request.PaymentType = Payment.Processor.PaymentType.Fee;
                    request.PaymentInstallmentNumber = 0;
                    var fundingData = await paymentService.AddFundingRequest(EntityType, loanDetails.LoanNumber, request);

                    schedule.IsProcessed = true;
                    await loanSchedule.UpdateFeeDetails(loanDetails.LoanNumber, schedule.Id, schedule);

                    if (schedule.FeeType.ToLower() == "scheduled")
                    {
                        var feeDetails = new ApplyFeeRequest();
                        feeDetails.FeeName = schedule.FeeId;
                        feeDetails.ApplyDate = tenantTime.Now.UtcDateTime;
                        feeDetails.FeeType = schedule.FeeType;
                        feeDetails.Description = "Scheduled Fee";
                        feeDetails.FeeAmount = schedule.FeeAmount;
                        await accrual.ApplyFee(loanDetails.LoanNumber, feeDetails);
                    }
                    await eventHub.Publish(new FeePullRequestCreated
                    {
                        EntityId = loanDetails.LoanNumber,
                        EntityType = EntityType,
                        Response = fundingData
                    });

                    await eventHub.Publish(new FEE_DUE
                    {
                        LoanNumber = loanDetails.LoanNumber,
                        EntityType = EntityType,
                        Response = fundingData
                    });
                    return fundingData;
                }
            }
            return null;
        }

    }
}
