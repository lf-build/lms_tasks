﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace LMS.Tasks.Loan.CreateFeePullRequest
{
    public class Configuration:IDependencyConfiguration
    {
        public int NotificationSendDays { get; set; }
        public List<string> LoanStatus { get; set; }

        public string CustomPurpose { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
