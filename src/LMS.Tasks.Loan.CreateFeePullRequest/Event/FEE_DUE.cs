﻿using LMS.Payment.Processor;

namespace LMS.Tasks.Loan.CreateFeePullRequest
{
    public class FEE_DUE
    {
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public IFunding Response { get; set; }
     
    }
}
