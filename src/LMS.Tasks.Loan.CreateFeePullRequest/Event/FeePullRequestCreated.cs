﻿using LMS.Payment.Processor;

namespace LMS.Tasks.Loan.CreateFeePullRequest
{
    public class FeePullRequestCreated
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public IFunding Response { get; set; }
     
    }
}
