﻿using LendFoundry.Security.Tokens;

namespace LMS.Tasks.Loan.CreateFeePullRequest
{
    public interface IFeeAgentFactory
    {
        Agent Create(ITokenReader reader);
    }
}